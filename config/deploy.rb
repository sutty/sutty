# frozen_string_literal: true

set :application, 'sutty'
set :repo_url, 'git@0xacab.org:sutty/sutty.git'

set :bundle_flags, '--deployment'
set :default_env, path: '/usr/lib/passenger/bin:$PATH'

set :linked_dirs, %w[_sites _deploy _usuarias _invitadxs public/covers]
set :linked_files, %w[.env db/production.sqlite3]

set :keep_releases, 1

before 'deploy:assets:precompile', 'deploy:yarn_install'

namespace :deploy do
  desc 'Run rake yarn:install'
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install")
      end
    end
  end
end
