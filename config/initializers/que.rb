# frozen_string_literal: true

ActiveJob::Serializers.add_serializers ActiveJob::Serializers::ExceptionSerializer

# Notificar los errores
Que.error_notifier = proc do |error, job|
  ExceptionNotifier.notify_exception(error, data: (job.dup || {}))
end
