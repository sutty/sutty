# frozen_string_literal: true

# CORS
#
# @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS}
# @see {https://github.com/cyu/rack-cors#origin}
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    # Queremos obtener un dominio válido y no permitir ningún otro
    # origen.  Como los sitios pueden tener muchos orígenes, necesitamos
    # una forma de encontrarlos por su URL.
    #
    # El problema sería que otros sitios con JS malicioso hagan pedidos
    # a nuestra API desde otros sitios infectados.
    #
    # XXX: La primera parte del dominio tiene que coincidir con el
    # nombre del sitio.
    #
    # XXX: Al terminar de entender esto nos pasó que el servidor recibe
    # la petición de todas maneras, con lo que no estamos previniendo
    # que nos hablen, sino que lean información.  Solo va a funcionar si
    # el servidor no tiene el Preflight cacheado.
    #
    # TODO: Limitar el acceso desde Nginx también.
    #
    # TODO: Poder consultar por sitios por todas sus URLs posibles.
    origins do |source, _|
      # Cacheamos la respuesta para no tener que volver a procesarla
      # cada vez.
      Rails.cache.fetch(source, expires_in: 1.hour) do
        uri = URI(source)

        if (name = uri&.host&.split('.', 2)&.first).present?
          Site.where(name: [name, uri.host + '.']).pluck(:name).first.present?
        else
          false
        end
      rescue URI::Error
        false
      end
    end

    resource '*', headers: :any, methods: %i[get post patch put]
  end
end
