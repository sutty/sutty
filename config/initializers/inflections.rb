# frozen_string_literal: true

ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.plural 'invitadx', 'invitadxs'
  inflect.singular 'invitadxs', 'invitadx'
  inflect.plural 'invitade', 'invitades'
  inflect.singular 'invitades', 'invitade'
  inflect.plural 'usuarie', 'usuaries'
  inflect.singular 'usuaries', 'usuarie'
  inflect.plural 'licencia', 'licencias'
  inflect.singular 'licencias', 'licencia'
  inflect.plural 'rol', 'roles'
  inflect.singular 'roles', 'rol'
  inflect.plural 'rollup', 'rollups'
  inflect.singular 'rollups', 'rollup'
  inflect.plural 'code_of_conduct', 'codes_of_conduct'
  inflect.singular 'codes_of_conduct', 'code_of_conduct'
  inflect.plural 'privacy_policy', 'privacy_policies'
  inflect.singular 'privacy_policies', 'privacy_policy'
end

ActiveSupport::Inflector.inflections(:es) do |inflect|
  inflect.plural 'invitadx', 'invitadxs'
  inflect.singular 'invitadxs', 'invitadx'
  inflect.plural 'invitade', 'invitades'
  inflect.singular 'invitades', 'invitade'
  inflect.plural 'usuarie', 'usuaries'
  inflect.singular 'usuaries', 'usuarie'
  inflect.plural 'rol', 'roles'
  inflect.singular 'roles', 'rol'
  inflect.plural 'licencia', 'licencias'
  inflect.singular 'licencias', 'licencia'
  inflect.plural 'rollup', 'rollups'
  inflect.singular 'rollups', 'rollup'
  inflect.plural 'code_of_conduct', 'codes_of_conduct'
  inflect.singular 'codes_of_conduct', 'code_of_conduct'
  inflect.plural 'privacy_policy', 'privacy_policies'
  inflect.singular 'privacy_policies', 'privacy_policy'
end
