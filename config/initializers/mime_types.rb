# frozen_string_literal: true

Mime::Type.register 'application/csp-report', :json
