# frozen_string_literal: true

String.include CoreExtensions::String::StripTags
String.include CoreExtensions::String::RemoveDiacritics
Jekyll::Document.include CoreExtensions::Jekyll::Document::Path
Jekyll::DataReader.include Jekyll::Readers::DataReaderDecorator

# Definir tags de Liquid que provienen de complementos para que siempre
# devuelvan contenido vacío.
%w[seo feed_meta turbolinks].each do |tag|
  Liquid::Template.register_tag(tag, Jekyll::Tags::Empty)
end

Liquid::Template.register_tag('base', Jekyll::Tags::Base)

module ActionDispatch
  # Redefinir el formateo de URLs de Rails para eliminar parámetros
  # selectivamente
  module Journey
    Formatter.class_eval do
      alias_method :generate_original, :generate

      # Eliminar el locale a menos que estemos generando la URL para un
      # Post.  Esto es para que las URLs no lleven un ?locale=XX
      # innecesario y además porque generan conflictos en la subida de
      # archivos de ActionText.
      def generate(name, options, path_parameters)
        options.delete(:locale) unless options[:controller] == 'posts'

        generate_original(name, options, path_parameters)
      end
    end
  end
end

# Lazy Loading de Jekyll, deshabilitando la instanciación de elementos
# que no necesitamos.  Esto permite que podamos leer el sitio por partes
# en lugar de todo junto.
#
# TODO: Aplicar monkey patches en otro lado...
module Jekyll
  Configuration.class_eval do
    # No agregar colecciones por defecto, solo las que digamos en base a
    # los idiomas.  Esto remueve la colección "posts".
    #
    # Las colecciones de idiomas son agregadas por Site.
    #
    # @see Site#configuration
    # @return [Jekyll::Configuration]
    def add_default_collections
      self
    end
  end

  Site.class_eval do
    def configure_theme
      self.theme = nil
      self.theme = Jekyll::Theme.new(config['theme'], self) unless config['theme'].nil?
    end
  end

  Reader.class_eval do
    # No necesitamos otros posts
    def retrieve_posts(_); end

    # No necesitamos otros directorios
    def retrieve_dirs(_, _, _); end

    # No necesitamos las páginas
    def retrieve_pages(_, _); end

    # No necesitamos los archivos estáticos
    def retrieve_static_files(_, _); end

    # Solo lee los datos, desde la plantilla y luego desde el sitio,
    # usando rutas absolutas, para evitar el uso confuso de Dir.chdir.
    #
    # Reemplaza jekyll-data también!
    #
    # @return [Hash]
    def read_data
      @site.data =
        begin
          reader = DataReader.new(site)
          theme_dir = site.in_theme_dir('_data')
          data_dir = site.in_source_dir(site.config['data_dir'])

          if theme_dir
            reader.read_data_to(theme_dir, reader.content)
            reader.read_data_to(data_dir, reader.content)
            reader.content
          else
            reader.read data_dir
          end
        end
    end

    # Lee los layouts
    def read_layouts
      @site.layouts = LayoutReader.new(site).read unless @site.layouts.present?
    end

    # Lee todos los artículos del sitio
    def read_collections
      read_directories
      read_included_excludes
      sort_files!
      CollectionReader.new(site).read
    end
  end

  Theme.class_eval do
    attr_reader :site

    def initialize(name, site)
      @name = name.downcase.strip
      @site = site
    end

    def root
      @root ||= begin
        lockfile = Bundler::LockfileParser.new(File.read(site.in_source_dir('Gemfile.lock')))
        spec = lockfile.specs.find do |spec|
          spec.name == name
        end

        unless spec
          I18n.with_locale(locale) do
            raise Jekyll::Errors::InvalidThemeName,
                  I18n.t('activerecord.errors.models.site.attributes.design_id.missing_gem', theme: name)
          rescue Jekyll::Errors::InvalidThemeName => e
            ExceptionNotifier.notify_exception(e, data: { theme: name, site: File.basename(site.source) })
            raise
          end
        end

        ruby_version = Gem::Version.new(RUBY_VERSION)
        ruby_version.canonical_segments[2] = 0
        base_path = Rails.root.join('_storage', 'gems', File.basename(site.source), 'ruby',
                                    ruby_version.canonical_segments.join('.'))

        File.realpath(
          case spec.source
          when Bundler::Source::Git
            File.join(base_path, 'bundler', 'gems', spec.source.extension_dir_name)
          when Bundler::Source::Rubygems
            File.join(base_path, 'gems', spec.full_name)
          end
        )
      end
    end

    def runtime_dependencies
      []
    end

    private

    def gemspec; end

    # @return [Symbol]
    def locale
      @locale ||= (site.config['locale'] || site.config['lang'] || I18n.locale).to_sym
    end
  end

  # No necesitamos los archivos de la plantilla
  ThemeAssetsReader.class_eval do
    def read; end
  end

  # Aplazar la lectura del documento
  Document.class_eval do
    alias_method :read!, :read
    def read; end

    # Permitir restablecer el documento sin crear uno nuevo
    def reset
      @path = @extname = @has_yaml_header = @relative_path = nil
      @basename_without_ext = @data = @basename = nil
      @renderer = @url_placeholders = @url = nil
      @to_liquid = @write_p = @excerpt_separator = @id = nil
      @related_posts = @cleaned_relative_path = self.content = nil
    end
  end

  # Prevenir la instanciación de Time
  #
  # @see {https://github.com/jekyll/jekyll/pull/8425}
  Utils.class_eval do
    def parse_date(input, msg = 'Input could not be parsed.')
      @parse_date_cache ||= {}
      @parse_date_cache[input] ||= Time.parse(input).localtime
    rescue ArgumentError
      raise Errors::InvalidDateError, "Invalid date '#{input}': #{msg}"
    end
  end
end

# No aplicar el orden por ranking para poder obtener los artículos en el
# orden que tendrían en el sitio final.
module PgSearch
  ScopeOptions.class_eval do
    def apply(scope)
      scope = include_table_aliasing_for_rank(scope)
      rank_table_alias = scope.pg_search_rank_table_alias(include_counter: true)

      scope.joins(rank_join(rank_table_alias))
    end
  end
end
