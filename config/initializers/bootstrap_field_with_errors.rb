# frozen_string_literal: true

# Adaptar la renderización de campos con error de Rails para que sea
# compatible con Bootstrap4.  La forma actual rompe los inputs
# personalizados.
ActionView::Base.field_error_proc = proc do |html_tag, _|
  return unless html_tag

  html = Nokogiri::HTML::DocumentFragment.parse html_tag
  html&.children&.first&.attributes&.dig('class')&.value += ' is-invalid'

  html&.to_s&.html_safe || html_tag
end
