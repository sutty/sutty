# frozen_string_literal: true

# Como el sitio puede contener puntos, no queremos confundirlos con
# tipos de contenido, por ejemplo que /sites/site.org no intente service
# text/org.
SITE_ID_RE = %r{[^/]+}.freeze

Rails.application.routes.draw do
  devise_for :usuaries, controllers: { registrations: 'registrations' }
  get '/.well-known/change-password', to: redirect('/usuaries/edit')

  require 'que/web'
  mount Que::Web => '/que'

  root 'application#index'

  constraints(Constraints::ApiSubdomain.new) do
    scope module: 'api' do
      namespace :v1 do
        resources :csp_reports, only: %i[create]

        namespace :activity_pub do
          resources :remote_flags, only: %i[show]
        end

        resources :sites, only: %i[index], constraints: { site_id: SITE_ID_RE, id: SITE_ID_RE } do
          get  :'invitades/cookie', to: 'invitades#cookie'
          post :'posts/:layout', to: 'posts#create', as: :posts

          get  :'contact/cookie', to: 'invitades#contact_cookie'
          post :'contact/:form', to: 'contact#receive', as: :contact

          namespace :webhooks do
            post :pull, to: 'pull#pull'

            scope :social_inbox do
              post :moderationqueued, to: 'social_inbox#moderationqueued'
              post :onapproved, to: 'social_inbox#onapproved'
              post :onrejected, to: 'social_inbox#onrejected'
            end
          end
        end
      end
    end
  end

  # Las rutas privadas empiezan con una ruta única para poder hacer un
  # alias en nginx sin tener que usar expresiones regulares para
  # detectar el nombre del sitio.
  get '/sites/private/:site_id(*file)', to: 'private#show', constraints: { site_id: SITE_ID_RE }
  get '/env.js', to: 'env#index'

  match '/api/v3/projects/:site_id/notices' => 'api/v1/notices#create', via: %i[post]

  resources :sites, constraints: { site_id: SITE_ID_RE, id: SITE_ID_RE } do
    # Gestionar actualizaciones del sitio
    get 'pull', to: 'sites#fetch'
    post 'pull', to: 'sites#merge'

    get 'status', to: 'sites#status'
    get 'button', to: 'sites#button'

    # Gestionar usuaries
    get 'usuaries/invite', to: 'usuaries#invite'
    post 'usuaries/invite', to: 'usuaries#send_invitations'
    patch 'usuaries/accept_invitation', to: 'usuaries#accept_invitation'
    patch 'usuaries/reject_invitation', to: 'usuaries#reject_invitation'
    resources :usuaries do
      patch 'demote', to: 'usuaries#demote'
      patch 'promote', to: 'usuaries#promote'
    end

    get 'collaborate', to: 'collaborations#collaborate'
    post 'collaborate', to: 'collaborations#accept_collaboration'

    get 'moderation_queue', to: 'moderation_queue#index'

    resources :instance_moderations, only: [] do
      patch :pause, to: 'instance_moderations#pause'
      patch :allow, to: 'instance_moderations#allow'
      patch :block, to: 'instance_moderations#block'
    end

    patch :instance_moderations_action_on_several, to: 'instance_moderations#action_on_several'
    patch :fediblock_states_action_on_several, to: 'fediblock_states#action_on_several'

    resources :actor_moderations, only: %i[show] do
      ActorModeration.events.each do |actor_event|
        patch actor_event, to: "actor_moderations##{actor_event}"
      end
    end

    patch :actor_moderations_action_on_several, to: 'actor_moderations#action_on_several'

    resources :activity_pub, only: [] do
      ActivityPub.events.each do |event|
        patch event, to: "activity_pubs##{event}"
      end
    end

    patch :activity_pubs_action_on_several, to: 'activity_pubs#action_on_several'

    # Gestionar artículos según idioma
    nested do
      scope '/(:locale)', constraint: /[a-z]{2}(-[A-Z]{2})?/ do
        post :'posts/reorder', to: 'posts#reorder'

        get :'posts/new_array', to: 'posts#new_array'
        get :'posts/new_array_value', to: 'posts#new_array_value'
        get :'posts/new_related_post', to: 'posts#new_related_post'
        get :'posts/new_has_one', to: 'posts#new_has_one'
        get :'posts/form', to: 'posts#form'
        get :'posts/modal', to: 'posts#modal'

        resources :posts do
          get 'p/:page', action: :index, on: :collection
          get :preview, to: 'posts#preview'
        end
      end
    end

    # Gestionar traducciones
    get 'i18n', to: 'i18n#index'
    get 'i18n/edit', to: 'i18n#edit'
    post 'i18n', to: 'i18n#update'

    # Ver límites de tamaño de archivos
    get 'limits', to: 'sites#limits'

    # Compilar el sitio
    post 'enqueue', to: 'sites#enqueue'
    post 'reorder_posts', to: 'sites#reorder_posts'

    resources :stats, only: [:index]
    get :'stats/host', to: 'stats#host'
    get :'stats/uris', to: 'stats#uris'
    get :'stats/resources', to: 'stats#resources'

    resources :build_stats, only: %i[index]
  end
end
