# frozen_string_literal: true

# Gestiona los filtros de ActivityPub
class ActivityPubProcessor < Rubanok::Processor
  # En orden descendiente para encontrar la última actividad
  #
  # Por ahora solo queremos moderar comentarios.
  prepare do
    raw
      .joins(:activities)
      .where(
        activity_pub_activities: {
          type: %w[ActivityPub::Activity::Create ActivityPub::Activity::Update]
        },
        object_type: %w[ActivityPub::Object::Note ActivityPub::Object::Article]
      ).order(updated_at: :desc)
  end

  map :activity_pub_state, activate_always: true do |activity_pub_state: 'paused'|
    raw.where(aasm_state: activity_pub_state)
  end
end
