# frozen_string_literal: true

# Gestiona los filtros de InstanceModeration
class InstanceModerationProcessor < Rubanok::Processor
  prepare do
    raw.includes(:instance).order('activity_pub_instances.hostname')
  end

  map :instance_state, activate_always: true do |instance_state: 'paused'|
    raw.where(aasm_state: instance_state)
  end
end
