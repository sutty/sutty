# frozen_string_literal: true

# Gestiona los filtros de ActorModeration
class ActorModerationProcessor < Rubanok::Processor
  # En orden descendiente para encontrar le últime Actor
  prepare do
    raw.order(updated_at: :desc)
  end

  map :actor_state, activate_always: true do |actor_state: 'paused'|
    raw.where(aasm_state: actor_state)
  end
end
