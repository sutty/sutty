# frozen_string_literal: true

# Política de acceso a artículos
class IndexedPostPolicy
  attr_reader :indexed_post, :usuarie, :site

  def initialize(usuarie, indexed_post)
    @usuarie = usuarie
    @indexed_post = indexed_post
    @site = indexed_post.site
  end

  def index?
    true
  end

  # Les invitades solo pueden ver sus propios posts
  def show?
    site.usuarie?(usuarie) || site.indexed_posts.by_usuarie(usuarie.id).find_by_post_id(indexed_post.post_id).present?
  end

  def preview?
    show?
  end

  def new?
    create?
  end

  def create?
    true
  end

  def edit?
    update?
  end

  # Les invitades solo pueden modificar sus propios artículos
  def update?
    show?
  end

  # Solo las usuarias pueden eliminar artículos.  Les invitades pueden
  # borrar sus propios artículos
  def destroy?
    update?
  end

  # Las usuarias pueden ver todos los posts
  #
  # Les invitades solo pueden ver sus propios posts
  class Scope
    attr_reader :usuarie, :scope

    def initialize(usuarie, scope)
      @usuarie = usuarie
      @scope = scope
    end

    def resolve
      return scope if scope&.first&.site&.usuarie? usuarie

      scope.by_usuarie(usuarie.id)
    end
  end
end
