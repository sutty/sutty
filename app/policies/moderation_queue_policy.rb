# frozen_string_literal: true

# Si la cola de moderación está activada y le usuarie tiene permisos de
# usuarie.
ModerationQueuePolicy = Struct.new(:usuarie, :moderation_queue) do
  def index?
    moderation_queue.site.moderation_enabled? && moderation_queue.site.usuarie?(usuarie)
  end
end
