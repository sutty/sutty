# frozen_string_literal: true

# Política de acceso a las estadísticas
class SiteStatPolicy
  attr_reader :site_stat, :usuarie

  def initialize(usuarie, site_stat)
    @usuarie = usuarie
    @site_stat = site_stat
  end

  def index?
    site_stat.site.usuarie? usuarie
  end

  def host?
    index?
  end

  def resources?
    index?
  end

  def uris?
    index?
  end
end
