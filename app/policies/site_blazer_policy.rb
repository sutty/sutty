# frozen_string_literal: true

# Les invitades no pueden ver las estadísticas (aun)
SiteBlazerPolicy = Struct.new(:usuarie, :site_blazer) do
  def home?
    site_blazer&.site&.usuarie? usuarie
  end

  alias_method :show?, :home?
end
