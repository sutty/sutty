# frozen_string_literal: true

# Solo les usuaries pueden moderar actores
ActorModerationPolicy = Struct.new(:usuarie, :actor_moderation) do
  ActorModeration.events.each do |actor_event|
    define_method(:"#{actor_event}?") do
      actor_moderation.site.usuarie? usuarie
    end
  end

  # En este paso tenemos varias cuentas por moderar pero todas son
  # del mismo sitio.
  def action_on_several?
    actor_moderation.first.site.usuarie? usuarie
  end
end
