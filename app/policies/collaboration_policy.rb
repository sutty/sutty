# frozen_string_literal: true

# Política de aceptación de colaboradorxs
CollaborationPolicy = Struct.new(:usuarie, :collaboration) do
  def collaborate?
    collaboration.site.colaboracion_anonima
  end

  def accept_collaboration?
    collaboration.site.colaboracion_anonima
  end
end
