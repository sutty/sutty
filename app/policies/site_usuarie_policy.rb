# frozen_string_literal: true

# Gestiona la relación entre sitios y usuaries
class SiteUsuariePolicy
  attr_reader :usuarie, :site_usuarie

  def initialize(usuarie, site_usuarie)
    @usuarie = usuarie
    @site_usuarie = site_usuarie
  end

  def index?
    usuarie?
  end

  # Les usuaries pueden remover a otres usuaries e invitades del sitio
  def destroy?
    usuarie?
  end

  # Les usuaries pueden convertir a otres usuaries en invitades
  def demote?
    usuarie?
  end

  def promote?
    usuarie?
  end

  def invite?
    usuarie?
  end

  def send_invitations?
    usuarie?
  end

  def accept_invitation?
    !!site_usuarie.usuarie.rol_for_site(site_usuarie.site)&.temporal
  end

  def reject_invitation?
    accept_invitation?
  end

  private

  def invitades?
    site_usuarie.site.invitades?
  end

  def usuarie?
    invitades? && site_usuarie.site.usuarie?(usuarie)
  end

  def invitade?
    site_usuarie.site.invitade? usuarie
  end
end
