# frozen_string_literal: true

# Solo les usuaries pueden moderar instancias
InstanceModerationPolicy = Struct.new(:usuarie, :instance_moderation) do
  InstanceModeration.events.each do |event|
    define_method(:"#{event}?") do
      instance_moderation.site.usuarie? usuarie
    end
  end

  # En este paso tenemos varias instancias por moderar pero todas son
  # del mismo sitio.
  def action_on_several?
    instance_moderation.first.presence && instance_moderation.first.site.usuarie?(usuarie)
  end
end
