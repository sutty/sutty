# frozen_string_literal: true

# Una política para mostrar un error 404 cuando no se puede encontrar el
# recurso.
#
# @see app/controllers/application_controller.rb
# @see app/controllers/posts_controller.rb
# @see app/controllers/site_controller.rb
class NilClassPolicy
  def initialize(_, _)
    raise Pundit::NilPolicyError
  end
end
