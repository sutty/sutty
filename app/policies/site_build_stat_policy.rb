# frozen_string_literal: true

# Quiénes pueden ver estados de compilación de un sitio
class SiteBuildStatPolicy
  attr_reader :site_build_stat, :usuarie

  def initialize(usuarie, site_build_stat)
    @usuarie = usuarie
    @site_build_stat = site_build_stat
  end

  # Todes les usuaries e invitades de este sitio
  def index?
    site_build_stat.site.usuarie?(usuarie) || site_build_stat.site.invitade?(usuarie)
  end
end
