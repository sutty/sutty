# frozen_string_literal: true

module ActiveStorage
  module AuthenticatedDirectUploadsController
    extend ActiveSupport::Concern

    included do
      before_action :authenticate_usuarie!
    end
  end
end
