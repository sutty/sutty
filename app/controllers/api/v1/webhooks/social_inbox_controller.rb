# frozen_string_literal: true

module Api
  module V1
    module Webhooks
      # Recibe webhooks de la Social Inbox
      #
      # @see {https://www.w3.org/TR/activitypub/}
      class SocialInboxController < BaseController
        include Api::V1::Webhooks::Concerns::WebhookConcern

        # Validar que el token sea correcto
        before_action :usuarie

        # Cuando una actividad ingresa en la cola de moderación, la
        # recibimos por acá
        #
        # Vamos a recibir Create, Update, Delete, Follow, Undo,
        # Announce, Like y obtener el objeto dentro de cada una para
        # guardar un estado asociado al sitio.
        #
        # El objeto del estado puede ser un objeto o une actore,
        # dependiendo de la actividad.
        def moderationqueued
          process! :paused

          head :accepted
        end

        # Cuando la Social Inbox acepta una actividad, la recibimos
        # igual y la guardamos por si cambiamos de idea.
        def onapproved
          process! :approved

          head :accepted
        end

        # Cuando la Social Inbox rechaza una actividad, la recibimos
        # igual y la guardamos por si cambiamos de idea.
        def onrejected
          process! :rejected

          head :accepted
        end

        private

        # Envía la actividad para procesamiento por separado.
        #
        # @param initial_state [Symbol]
        def process!(initial_state)
          ::ActivityPub::ProcessJob
            .set(wait: ApplicationJob.random_wait)
            .perform_later(site: site, body: request.raw_post, initial_state: initial_state)
        end
      end
    end
  end
end
