# frozen_string_literal: true

module Api
  module V1
    # Recibe los reportes de Content Security Policy
    class CspReportsController < BaseController
      skip_forgery_protection

      # No queremos indicar que algo salió mal
      rescue_from ActionController::ParameterMissing, with: :csp_report_created

      # Crea un reporte de CSP intercambiando los guiones medios por
      # bajos
      #
      # TODO: Aplicar rate_limit
      def create
        csp = CspReport.new(csp_report_params.to_h.transform_keys do |k|
                              k.tr('-', '_')
                            end)

        csp.id = SecureRandom.uuid
        csp.save

        csp_report_created
      end

      private

      # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only#Violation_report_syntax
      def csp_report_params
        params.require(:'csp-report')
              .permit(:disposition,
                      :referrer,
                      :'blocked-uri',
                      :'document-uri',
                      :'effective-directive',
                      :'original-policy',
                      :'script-sample',
                      :'status-code',
                      :'violated-directive',
                      :'line-number',
                      :'column-number',
                      :'source-file')
      end

      def csp_report_created
        render json: {}, status: :created
      end
    end
  end
end
