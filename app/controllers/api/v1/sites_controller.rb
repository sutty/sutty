# frozen_string_literal: true

module Api
  module V1
    # API para sitios
    class SitesController < BaseController
      SUBDOMAIN = ".#{Site.domain}"
      TESTING_SUBDOMAIN = ".testing.#{Site.domain}"
      PARTS = Site.domain.split('.').count

      if Rails.env.production?
        http_basic_authenticate_with name: ENV['HTTP_BASIC_USER'],
                                    password: ENV['HTTP_BASIC_PASSWORD']
      end

      # Lista de nombres de dominios a emitir certificados
      def index
        all_names = sites_names.concat(alternative_names).concat(www_names).concat(api_names).uniq.map do |name|
          canonicalize name
        end.reject do |name|
          subdomain? name
        end.reject do |name|
          testing? name
        end.uniq

        render json: all_names
      end

      private

      # @param query [ActiveRecord::Relation]
      # @return [Array<String>]
      def hostname_of(query)
        query.pluck(Arel.sql("values->>'hostname'")).compact.uniq
      end

      def canonicalize(name)
        name.end_with?('.') ? name[0..-2] : "#{name}.#{Site.domain}"
      end

      # Es un subdominio directo del dominio principal
      #
      # @param name [String]
      # @return [Bool]
      def subdomain?(name)
        name.end_with?(SUBDOMAIN) && name.split('.').count == (PARTS + 1)
      end

      # Es un dominio de prueba
      #
      # @param name [String]
      # @return [Bool]
      def testing?(name)
        name.end_with?(TESTING_SUBDOMAIN) && name.split('.').count == (PARTS + 2)
      end

      # Nombres de los sitios
      #
      # @param name [String]
      # @return [Array<String>]
      def sites_names
        Site.all.order(:name).pluck(:name)
      end

      # Dominios alternativos, incluyendo todas las clases derivadas de
      # esta.
      #
      # @return [Array<String>]
      def alternative_names
        hostname_of(DeployAlternativeDomain.all)
      end

      # Obtener todos los sitios con API habilitada, es decir formulario
      # de contacto y/o colaboración anónima.
      #
      # @return [Array<String>]
      def api_names
        Site.where(contact: true)
            .or(Site.where(colaboracion_anonima: true))
            .pluck(:name).map do |name|
              "api.#{name}"
            end
      end

      # Todos los dominios con WWW habilitado
      def www_names
        Site.where(id: DeployWww.all.pluck(:site_id)).pluck(:name).map do |name|
          "www.#{name}"
        end
      end
    end
  end
end
