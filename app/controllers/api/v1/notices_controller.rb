# frozen_string_literal: true

module Api
  module V1
    # Recibe notificaciones desde Airbrake
    class NoticesController < BaseController
      skip_before_action :verify_authenticity_token

      # Generar un stacktrace en segundo plano y enviarlo por correo
      # solo si la API key es verificable.  Del otro lado siempre
      # respondemos con lo mismo.
      def create
        if (site&.airbrake_valid? airbrake_token) && !detected_device.bot?
          BacktraceJob.perform_later site: site,
                                     params: airbrake_params.to_h
        end

        render status: 201, json: { id: 1, url: '' }
      end

      private

      # XXX: Por alguna razón Airbrake envía los datos con Content-Type:
      # text/plain.
      def airbrake_params
        @airbrake_params ||=
          params.merge!(FastJsonparser.parse(request.raw_post) || {})
                .permit(
                  {
                    errors: [
                      :type,
                      :message,
                      { backtrace: %i[file line column function] }
                    ]
                  },
                  {
                    context: [
                      :url,
                      :language,
                      :severity,
                      :userAgent,
                      :windowError,
                      :rootDirectory,
                      {
                        history: [
                          :date,
                          :type,
                          :severity,
                          :target,
                          :method,
                          :duration,
                          :statusCode,
                          { arguments: [] }
                        ]
                      }
                    ]
                  }
                )
      end

      def site
        @site ||= Site.find(params[:site_id])
      rescue ActiveRecord::RecordNotFound
      end

      def airbrake_token
        @airbrake_token ||= params[:key]
      end

      # @return [DeviceDetector]
      def detected_device
        @detected_device ||= DeviceDetector.new(request.headers['User-Agent'], request.headers)
      end
    end
  end
end
