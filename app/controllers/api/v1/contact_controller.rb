# frozen_string_literal: true

module Api
  module V1
    # API para formulario de contacto
    class ContactController < ProtectedController
      # Recibe un mensaje a través del formulario de contacto y lo envía
      # a les usuaries del sitio.
      #
      # Tenemos que verificar que el sitio exista y que algunos campos
      # estén llenos para detener spambots o DDOS.  También nos vamos a
      # estar apoyando en la limitación de peticiones en el servidor web.
      def receive
        # No hacer nada si no se pasaron los chequeos
        return if performed?

        # TODO: Verificar que los campos obligatorios hayan llegado!

        # Si todo salió bien, enviar los correos y redirigir al sitio.
        # El sitio nos dice a dónde tenemos que ir.
        ContactJob.perform_later site,
                                 params[:form],
                                 contact_params.to_h.symbolize_keys,
                                 params[:redirect]

        redirect_to params[:redirect] || origin.to_s
      end

      private

      def from_is_address?
        return if contact_params[:from].blank?
        return if EmailAddress.valid? contact_params[:from]

        @reason = 'email_invalid'
        head :precondition_required
      end

      def gave_consent?
        return if contact_params[:consent].present?

        @reason = 'no_consent'
        render plain: Rails.env.production? ? nil : @reason, status: :precondition_required
      end

      # Los campos que se envían tienen que corresponder con un
      # formulario de contacto.
      def destination_exists?
        return if form? && site.form?(params[:form])

        @reason = 'form_doesnt_exist'
        render plain: Rails.env.production? ? nil : @reason, status: :precondition_required
      end

      # Parámetros limpios
      def contact_params
        @contact_params ||= params.permit(site.form(params[:form]).params)
      end
    end
  end
end
