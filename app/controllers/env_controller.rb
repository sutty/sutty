# frozen_string_literal: true

class EnvController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def index
    @site = Site.find_by_name('panel') || Site.first

    stale? @site if @site
  end
end
