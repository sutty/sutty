# frozen_string_literal: true

# Gestiona las versiones privadas de los sitios.  Solo se puede acceder
# con una cuenta
class PrivateController < ApplicationController
  # XXX: Permite ejecutar JS
  skip_forgery_protection

  # Enviar el archivo si existe, agregar una / al final siempre para no
  # romper las direcciones relativas.
  def show
    authorize site

    # Detectar si necesitamos una / al final
    if needs_trailing_slash?
      redirect_to request.url + '/'
      return
    end

    raise PageNotFound if deploy_private && File.exist?(path)

    send_file path, disposition: 'inline'
  end

  private

  # Detects if the URL should have a trailing slash
  def needs_trailing_slash?
    !trailing_slash? && params[:format].blank?
  end

  def trailing_slash?
    request.env['REQUEST_URI'].ends_with?('/')
  end

  def site
    @site ||= find_site
  end

  def deploy_private
    @deploy_private ||= site.deploys.find_by(type: 'DeployPrivate')
  end

  # Devuelve la ruta completa del archivo
  def path
    @path ||= Pathname.new(File.join(deploy_private.destination, file)).realpath.to_s
  rescue Errno::ENOENT
    site_not_found_path
  end

  # Devuelve la ruta del archivo, limpieza copiada desde Jekyll
  #
  # @see Jekyll::URL#sanitize_url
  def file
    return @file if @file

    @file  = params[:file] || '/'
    @file += '/' if trailing_slash?
    @file += if @file.ends_with? '/'
               'index.html'
             else
               '.' + params[:format].to_s
             end

    @file = @file.gsub('..', '/').gsub('./', '').squeeze('/')
  end

  # Devuelve una página 404.html
  def site_not_found_path
    @site_not_found_path ||= File.join(deploy_private.destination, '404.html')
  end
end
