# frozen_string_literal: true

module ActiveStorage
  # Modificar {DiskController} para poder asociar el blob a un sitio
  module DiskControllerDecorator
    extend ActiveSupport::Concern

    included do
      alias_method :original_show, :show

      # Permitir incrustar archivos subidos (especialmente PDFs) desde
      # otros sitios.
      def show
        original_show.tap do |_|
          response.headers.delete 'X-Frame-Options'
        end
      end

      rescue_from ActiveRecord::RecordNotFound, with: :page_not_found

      # Asociar el archivo subido al sitio correspondiente.  Cada sitio
      # tiene su propio servicio de subida de archivos.
      def update
        if (token = decode_verified_token)
          if acceptable_content?(token)
            blob = ActiveStorage::Blob.find_by_key! token[:key]
            site = Site.find_by_name! token[:service_name]

            if remote_file?(token)
              url = request.body.read
              body = Down.download(url, max_size: 111.megabytes)
              checksum = Digest::MD5.file(body.path).base64digest
              blob.metadata[:url] = url
              blob.update_columns checksum: checksum, byte_size: body.size, metadata: blob.metadata
            else
              body = request.body
              checksum = token[:checksum]
            end

            named_disk_service(site.name).upload(token[:key], body, checksum:)

            site.static_files.attach(blob)
          else
            head :unprocessable_entity
          end
        else
          head :not_found
        end
      rescue ActiveRecord::ActiveRecordError, ActiveStorage::Error => e
        ExceptionNotifier.notify_exception(e, data: { token: })

        head :unprocessable_entity
      rescue Down::Error => e
        ExceptionNotifier.notify_exception(e, data: { key: token[:key], url: url, site: site.name })

        head :payload_too_large
      end

      # Define una ruta que lleva a un JSON con los valores de los tamaños
      # máximos de subida y descarga (respectivamente) que puede tener un
      # archivo.
      def limits
        ruby_hash = { max_upload_size: max_upload_size, max_download_size: max_download_size }
        render json: ruby_hash
      end

      private

      def remote_file?(token)
        token[:content_type] == 'sutty/download-from-url'
      end

      def page_not_found(exception)
        head :not_found

        params.permit!

        ExceptionNotifier.notify_exception(exception, data: { params: params.to_hash })
      end

      # Convierte a la variable de entorno que define el máximo tamaño de subida que puede tener un archivo
      # en un entero expresado en megabytes.
      #
      # @return [Integer] tamaño máximo de subida que puede tener un archivo, expresado en megabytes.
      def max_upload_size
        ENV.fetch('MAX_UPLOAD_SIZE', '111').to_i.megabytes
      end

      # Convierte a la variable de entorno que define el máximo tamaño de descarga que puede tener un archivo
      # en un entero expresado en megabytes.
      #
      # @return [Integer] tamaño máximo de descarga que puede tener un archivo, expresado en megabytes.
      def max_download_size
        ENV.fetch('MAX_DOWNLOAD_SIZE', '111').to_i.megabytes
      end
    end
  end
end

ActiveStorage::DiskController.include ActiveStorage::DiskControllerDecorator
