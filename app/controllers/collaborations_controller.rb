# frozen_string_literal: true

# Controlador para gestionar colaboraciones abiertas
#
# No necesitamos autenticación aun
class CollaborationsController < ApplicationController
  include Pundit
  include StrongParamsHelper

  def collaborate
    @site = Site.find_by_name(pluck_param(:site_id))
    authorize Collaboration.new(@site)

    @invitade = current_usuarie || @site.usuaries.build
  end

  # Aceptar una colaboración tiene varios pasos
  #
  # * Si le usuarie no existe, hay que crearle
  #
  # * Si le usuarie existe, hay que darle un rol si no lo tiene
  #
  # * Si le usuarie existe y no está logueade, pedirle la contraseña
  def accept_collaboration
    @site = Site.find_by_name(pluck_param(:site_id))
    authorize Collaboration.new(@site)

    @invitade = current_usuarie
    @invitade ||= logged_out_invitade

    # Salir si no se pudo validar le invitade
    return if @invitade == false

    Usuarie.transaction do
      # Si la cuenta no existe, crearla
      @invitade ||= Usuarie.create(invitade_params)
      # Y asignar un rol si no existe
      unless @invitade.rol_for_site(@site)
        @site.roles << Rol.create(site: @site, usuarie: @invitade,
                                  temporal: false, rol: 'invitade')
      end

      sign_in :usuarie, @invitade unless current_usuarie

      redirect_to site_path(@site)
    end
  end

  private

  def invitade_params
    params.require(:usuarie).permit(:email, :password)
  end

  def logged_out_invitade
    invitade = Usuarie.find_by_email(invitade_params[:email])
    return unless invitade

    # Si la contraseña no es válida, volver atrás
    if invitade.can_sign_in? invitade_params[:password]
      invitade
    else
      invitade.increment_and_lock!

      flash[:warning] = I18n.t('collaborations.password.incorrect')
      redirect_to site_collaborate_path(@site)

      false
    end
  end
end
