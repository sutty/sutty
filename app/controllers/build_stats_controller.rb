# frozen_string_literal: true

# La lista de estados de compilación, por ahora solo mostramos el último
# estado.
class BuildStatsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::DateHelper

  before_action :authenticate_usuarie!

  breadcrumb -> { current_usuarie.email }, :edit_usuarie_registration_path
  breadcrumb 'sites.index', :sites_path, match: :exact
  breadcrumb -> { site.title }, -> { site_posts_path(site, locale: locale) }, match: :exact

  def index
    authorize SiteBuildStat.new(site)
    breadcrumb I18n.t('build_stats.index.title'), ''

    @headers = %w[type url seconds size].map do |header|
      t("deploy_mailer.deployed.th.#{header}")
    end

    @table = site.deployment_list.map do |deploy|
      type = deploy.class.name.underscore
      urls = deploy.urls.map do |url|
        URI.parse(url)
      rescue URI::Error
        nil
      end.compact

      urls = [nil] if urls.empty?
      build_stat = deploy.build_stats.where(status: true).last
      seconds = build_stat&.seconds || 0

      {
        title: t("deploy_mailer.deployed.#{type}.title"),
        urls: urls,
        seconds: {
          human: distance_of_time_in_words(seconds),
          machine: "PT#{seconds}S"
        },
        size: number_to_human_size(build_stat&.bytes || 0, precision: 2)
      }
    end
  end
end
