# frozen_string_literal: true

# Modificaciones locales al registro de usuaries
#
# @see {https://github.com/heartcombo/devise/wiki/How-To:-Use-Recaptcha-with-Devise}
class RegistrationsController < Devise::RegistrationsController
  class SpambotError < StandardError; end

  PRIVATE_HEADERS = /(cookie|secret|token)/i

  prepend_before_action :anti_spambot_traps, only: %i[create]
  prepend_after_action :lock_spambots, only: %i[create]

  private

  # Condiciones bajo las que consideramos que un registro viene de unx
  # spambot
  #
  # @return [Bool]
  def spambot?
    @spambot ||= params.dig(:usuarie, :name).present?
  end

  # Bloquea las cuentas de spam dentro de un minuto, para hacerles creer
  # que la cuenta se creó correctamente.
  def lock_spambots
    return unless spambot?
    return unless current_usuarie

    LockUsuarieJob.set(wait: 1.minute).perform_later(usuarie: current_usuarie)
  end

  # Detecta e informa spambots muy simples
  #
  # @return [nil]
  def anti_spambot_traps
    raise SpambotError if spambot?
  rescue SpambotError => e
    ExceptionNotifier.notify_exception(e, data: { params: anonymized_params, headers: anonymized_headers })
    nil
  end

  # Devuelve parámetros anonimizados para prevenir filtrar la contraseña
  # de falsos positivos.
  #
  # @return [Hash]
  def anonymized_params
    params.except(:authenticity_token).permit!.to_h.tap do |p|
      p['usuarie'].delete 'password'
      p['usuarie'].delete 'password_confirmation'
    end
  end

  # Devuelve los encabezados de la petición sin información sensible de
  # Rails
  #
  # @return [Hash]
  def anonymized_headers
    request.headers.to_h.select do |_, v|
      v.is_a? String
    end.reject do |k, _|
      k =~ PRIVATE_HEADERS
    end
  end

  # Si le usuarie es considerade spambot, no enviamos el correo de
  # confirmación al crear la cuenta.
  def sign_up_params
    if spambot?
      params[:usuarie][:confirmed_at] = Time.now.utc

      devise_parameter_sanitizer.permit(:sign_up, keys: %i[confirmed_at])
    end

    super
  end
end
