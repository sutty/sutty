# frozen_string_literal: true

# Representa un objeto git LFS
class LfsObjectService
  attr_reader :site, :blob

  # @param :site [Site]
  # @param :blob [ActiveStorage::Blob]
  def initialize(site:, blob:)
    @site = site
    @blob = blob
  end

  def process
    # Crear el directorio
    FileUtils.mkdir_p(File.dirname(object_path))

    # Mover el archivo
    FileUtils.mv(path, object_path) unless File.exist? object_path

    # Crear el pointer
    Site::Writer.new(site: site, file: path, content: pointer).save

    # Commitear el pointer
    site.repository.commit(add: [path], usuarie: author, message: File.basename(path))

    # Eliminar el pointer
    FileUtils.rm(path)

    # Hacer link duro del objeto al archivo
    FileUtils.ln(object_path, path)
  end

  # @return [String]
  def path
    @path ||= blob.service.path_for(blob.key)
  end

  # @return [String]
  def digest
    @digest ||= Digest::SHA256.file(path).hexdigest
  end

  # @return [String]
  def object_path
    @object_path ||= File.join(site.path, '.git', 'lfs', 'objects', digest[0..1], digest[2..3], digest)
  end

  # @return [Integer]
  def size
    @size ||= File.size(File.exist?(object_path) ? object_path : path)
  end

  # @return [String]
  def pointer
    @pointer ||=
      <<~POINTER
        version https://git-lfs.github.com/spec/v1
        oid sha256:#{digest}
        size #{size}
      POINTER
  end

  def author
    @author ||= GitAuthor.new email: "disk_service@#{Site.domain}", name: 'DiskService'
  end
end
