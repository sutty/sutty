# frozen_string_literal: true

module ModerationQueueHelper
  def filter_states(**args)
    params.permit(:instance_state, :actor_state, :activity_pub_state).merge(**args)
  end

  def active?(states, state_name, state)
    if params[state_name].present?
      params[state_name] == state.to_s
    else
      states.first == state
    end
  end
end
