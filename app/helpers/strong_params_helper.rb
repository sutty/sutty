# frozen_string_literal: true

# Métodos reutilizables para trabajar con StrongParams
module StrongParamsHelper

  # Obtiene el valor de un param
  #
  # @todo No hay una forma mejor de hacer esto?
  # @param param [Symbol]
  # @param :optional [Bool]
  # @return [nil,String]
  def pluck_param(param, optional: false)
    if optional
      params.permit(param).values.first.presence
    else
      params.require(param).presence
    end
  end
end
