- unless site.errors.empty?
  - title = t('.errors.title')
  - help = t('.errors.help')
  = render 'bootstrap/alert' do
    %h4= title
    %p.lead= help
    %ul
      - site.errors.messages.each_pair do |attr, error|
        - attr = attr.to_s
        - error.each do |e|
          %li= link_to t('activerecord.attributes.site.' + attr) + ' ' + e, '#' + attr

= form_for site, html: { class: form_class(site) } do |f|
  - unless site.persisted?
    .form-group#name
      %h2= f.label :name
      %p.lead= sanitize_markdown t('.help.name'), tags: %w[strong]
      -#
        El dominio contiene letras y números
        No puede empezar ni terminar con guiones
        No puede estar compuesto solo de números

      = f.text_field :name,
        class: form_control(site, :name),
        required: true,
        pattern: '^([a-z0-9][a-z0-9\-]*)?[a-z0-9\.](\.testing)?$',
        minlength: 1,
        maxlength: 63
      - if invalid? site, :name
        .invalid-feedback= site.errors.messages[:name].join(', ')

  .form-group#title
    %h2= f.label :title
    %p.lead= t('.help.title')
    = f.text_field :title, class: form_control(site, :title),
      required: true
    - if invalid? site, :title
      .invalid-feedback= site.errors.messages[:title].join(', ')

  .form-group#description
    %h2= f.label :description
    %p.lead= t('.help.description')
    = f.text_area :description, class: form_control(site, :description),
      maxlength: 160, minlength: 10, required: true
    - if invalid? site, :description
      .invalid-feedback= site.errors.messages[:description].join(', ')
    %hr/

  - unless site.persisted?
    .form-group#design_id
      %h2= t('.design.title')
      %p.lead= t('.help.design')
      - if invalid? site, :design_id
        = render 'bootstrap/alert' do
          = t('activerecord.errors.models.site.attributes.design_id.layout_incompatible.help',
            layouts: site.incompatible_layouts.to_sentence)
      .row.row-cols-1.row-cols-md-2.designs
        -# Demasiado complejo para un f.collection_radio_buttons
        - Design.all.order(priority: :desc).each do |design|
          .design.col.d-flex.flex-column
            .custom-control.custom-radio
              = f.radio_button :design_id, design.id,
                checked: design.id == site.design_id,
                disabled: design.disabled,
                required: true, class: 'custom-control-input'
              = f.label "design_id_#{design.id}", design.name,
                class: 'custom-control-label font-weight-bold'
            .flex-fill
              = sanitize_markdown design.description,
                tags: %w[p a strong em]

            .btn-group{ role: 'group', 'aria-label': t('.design.actions') }
              - if design.url
                = link_to t('.design.url'), design.url,
                  target: '_blank', class: 'btn btn-secondary'
              - if design.license
                = link_to t('.design.license'), design.license,
                  target: '_blank', class: 'btn btn-secondary'
      %hr/

  .form-group.licenses#license_id
    %h2= t('.licencia.title')
    %p.lead= t('.help.licencia')
    - Licencia.all.find_each do |licencia|
      - next if licencia.custom? && site.licencia != licencia
      .row.license
        .col
          .media.mt-1
            - unless licencia.custom?
              = image_tag licencia.icons, alt: licencia.name, class: 'mr-3 mt-4'
            .media-body
              .custom-control.custom-radio
                = f.radio_button :licencia_id, licencia.id,
                  checked: licencia.id == site.licencia_id,
                  required: true, class: 'custom-control-input'
                = f.label "licencia_id_#{licencia.id}", class: 'custom-control-label font-weight-bold' do
                  = licencia.name
              = sanitize_markdown licencia.description,
                tags: %w[p a strong em ul ol li h1 h2 h3 h4 h5 h6]

              - unless licencia.custom?
                = link_to t('.licencia.url'), licencia.url, target: '_blank', class: 'btn btn-secondary', rel: 'noopener'

          %hr/

  .form-group
    %h2= t('.privacidad.title')
    %p.lead= sanitize_markdown t('.help.privacidad'), tags: %w[a]

    %hr/

  - if site.persisted?
    .form-group#tienda
      %h2= t('.tienda.title')
      %p.lead
        - if site.tienda?
          = t('.tienda.help')
        - else
          = t('.tienda.first_time_html')

      .row
        .col
          .form-group
            = f.label :tienda_url
            = f.url_field :tienda_url, class: 'form-control'
        .col
          .form-group
            = f.label :tienda_api_key
            = f.text_field :tienda_api_key, class: 'form-control'

      %hr/

    .form-group#contact
      %h2= t('.contact.title')
      %p.lead= t('.contact.help')

      .custom-control.custom-switch
        = f.check_box :contact, class: 'custom-control-input'
        = f.label :contact, class: 'custom-control-label'

    .form-group#acepta_invitades
      %h2= t('.acepta_invitades.title')
      %p.lead= t('.acepta_invitades.help')

      .custom-control.custom-switch
        = f.check_box :acepta_invitades, class: 'custom-control-input'
        = f.label :acepta_invitades, class: 'custom-control-label'

    .form-group#colaboracion_anonima
      %h2= t('.colaboracion_anonima.title')
      %p.lead= t('.colaboracion_anonima.help')

      .custom-control.custom-switch
        = f.check_box :colaboracion_anonima, class: 'custom-control-input'
        = f.label :colaboracion_anonima, class: 'custom-control-label'

    .form-group
      %h2= t('.deploys.title')
      %p.lead= t('.help.deploys')

      = f.fields_for :deploys do |deploy|
        = render "deploys/#{deploy.object.type.underscore}",
          deploy: deploy, site: site

  .form-group
    = f.submit submit, class: 'btn btn-secondary btn-lg btn-block'
