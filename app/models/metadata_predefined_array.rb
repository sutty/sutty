# frozen_string_literal: true

# Una lista de valores predefinidos
class MetadataPredefinedArray < MetadataArray
  def values
    @values ||= layout.dig(:metadata, name, 'values')&.map do |k, v|
      [v[I18n.locale.to_s], k]
    end&.to_h
  end

  # Devolver los valores legibles por humanes
  #
  # @todo Debería devolver los valores en el idioma del post, no de le
  # usuarie
  # @return [String]
  def to_s
    values.invert.select { |x, k| value.include?(x) }.values.join(', ')
  end
end
