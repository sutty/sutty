# frozen_string_literal: true

# Interfaz nueva para uno a muchos
class MetadataNewHasMany < MetadataHasMany
  include Metadata::UnusedValuesConcern
end
