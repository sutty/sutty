# frozen_string_literal: true

# El título es obligatorio para todos los Post, si el esquema no lo
# incluye, tenemos que poder generar un valor legible por humanes.
class MetadataTitle < MetadataString
  def titleize?
    false
  end

  # Siempre recalcular el título
  def value
    self[:value] = default_value
  end

  # Obtener todos los valores de texto del artículo y generar un título
  # en base a eso.
  #
  # @return [String]
  def default_value
    post.attributes.select do |attr|
      post[attr].titleize?
    end.map do |attr|
      post[attr].to_s
    end.compact.join(' ').strip.squeeze(' ')
  end
end
