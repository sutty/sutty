# frozen_string_literal: true

# Nueva interfaz para arrays predefinidos
class MetadataNewPredefinedArray < MetadataPredefinedArray; end
