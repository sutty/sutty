# frozen_string_literal: true

# Este campo representa el archivo donde se almacenan los datos
class MetadataPath < MetadataTemplate
  # :label en este caso es el idioma/colección
  #
  # @return [String]
  def default_value
    File.join(site.path, "_#{lang}", "#{limited_name}#{ext}")
  end

  # La ruta del archivo según Jekyll
  #
  # @return [String]
  def document_value
    document.path
  end

  def value
    self[:value] ||= self.value =
      if post.new?
        default_value
      else
        document_value
      end
  end
  alias absolute value
  alias to_s value

  def relative
    Pathname.new(value).relative_path_from(Pathname.new(site.path)).to_s
  end

  def basename
    File.basename(value, ext)
  end

  # No lo aceptamos en los parámetros
  def to_param; end

  private

  def sanitize(value)
    Pathname.new(value.to_s).relative_path_from(Pathname.new(site.path)).to_s.gsub('..', '').squeeze('/')
  end

  def ext
    document.data['ext'].blank? ? '.markdown' : document.data['ext']
  end

  def slug
    post.slug.value
  end

  def date
    post.date.value.strftime('%F')
  end

  # Limita el nombre de archivo a 255 bytes, de forma que siempre
  # podemos guardarlo
  #
  # @return [String]
  def limited_name
    "#{date}-#{slug}".mb_chars.limit(255 - ext.length)
  end
end
