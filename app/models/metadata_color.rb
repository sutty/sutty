# frozen_string_literal: true

# Un campo de color
class MetadataColor < MetadataString
  private

  def sanitize(value)
    @@color_re ||= %r{\A#[a-f0-9]{6}}.freeze

    if value.to_s =~ @@color_re
      value.to_s
    else
      document_value || default_value
    end
  end
end
