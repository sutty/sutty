# frozen_string_literal: true

# Une autore de commits
GitAuthor = Struct.new :email, :name, keyword_init: true
