# frozen_string_literal: true

# Nueva interfaz para relaciones muchos a muchos
class MetadataNewHasAndBelongsToMany < MetadataHasAndBelongsToMany; end
