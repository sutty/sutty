# frozen_string_literal: true

# Este metadato permite generar rutas manuales.
class MetadataPermalink < MetadataString
  # Los permalinks nunca pueden ser privados
  def private?
    false
  end

  # Devuelve la URL actual del sitio
  #
  # @return [String, nil]
  def default_value
    if post.new?
      super.presence
    else
      document.url
    end
  end

  private

  # Al hacer limpieza, validamos la ruta.  Eliminamos / multiplicadas,
  # puntos suspensivos, la primera / para que siempre sea relativa y
  # agregamos una / al final si la ruta no tiene extensión.
  #
  # @param value [String,nil]
  # @return [String,nil]
  def sanitize(value)
    return if value.nil?
    return value.strip if value.blank?

    value  = value.strip.unicode_normalize.gsub('..', '/').gsub('./', '')
    value  = value[1..-1] if value.start_with? '/'
    value += '/' if File.extname(value).blank?
    value  = value.squeeze('/')

    value
  end
end
