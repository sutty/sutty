# frozen_string_literal: true

# Un campo de idioma
class MetadataLang < MetadataTemplate
  def default_value
    super || I18n.locale
  end

  # @return [Symbol]
  def document_value
    document.collection.label.to_sym
  end

  def value
    self[:value] ||= self.value = document_value || default_value
  end

  def values
    site.locales
  end

  private

  # @param value [any]
  # @return [String]
  def sanitize(value)
    if site.locales.include?(value.to_s.to_sym)
      value
    else
      site.default_locale
    end.to_s
  end
end
