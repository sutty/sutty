# frozen_string_literal: true

# Nueva interfaz para relaciones 1:1
class MetadataNewHasOne < MetadataHasOne; end
