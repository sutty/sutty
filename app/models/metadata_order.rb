# frozen_string_literal: true

# Un campo de orden
class MetadataOrder < MetadataTemplate
  # El valor según la posición del post en la relación ordenada por
  # fecha, a fecha más alta, posición más alta
  def default_value
    super || ((site.indexed_posts.where(locale: locale).first&.order || 0) + 1)
  end

  # El orden nunca puede ser privado
  def private?
    false
  end

  private

  def sanitize(value)
    if value.respond_to?(:to_i)
      value.to_i
    else
      default_value
    end
  end
end
