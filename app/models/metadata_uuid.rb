# frozen_string_literal: true

# Asigna un identificador único al artículo
class MetadataUuid < MetadataTemplate
  def default_value
    SecureRandom.uuid
  end

  def private?
    false
  end

  private

  # TODO: Chequear que sea única, no nos importa el formato?
  #
  # @param value [any]
  # @return [String]
  def sanitize(value)
    sanitize_string(value.to_s)
  end
end
