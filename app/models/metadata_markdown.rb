# frozen_string_literal: true

# Markdown
class MetadataMarkdown < MetadataText
  # Renderizar a HTML y sanitizar
  def to_s
    sanitize CommonMarker.render_doc(value, %i[FOOTNOTES SMART],
                                     %i[table strikethrough autolink]).to_html
  end

  private

  # XXX: No sanitizamos acá porque se escapan varios símbolos de
  # markdown y se eliminan autolinks.  Mejor es habilitar la generación
  # SAFE de CommonMark en la configuración del sitio.
  def sanitize(string)
    string.unicode_normalize
  end
end
