# frozen_string_literal: true

# Clase genérica a través de la que podemos obtener la relación entre
# sitio y usuarie
SiteUsuarie = Struct.new(:site, :usuarie)
