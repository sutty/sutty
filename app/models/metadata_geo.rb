# frozen_string_literal: true

# Define un campo de coordenadas geográficas
class MetadataGeo < MetadataTemplate
  def default_value
    super || { 'lat' => nil, 'lng' => nil }
  end

  def empty?
    value == default_value
  end

  def save
    return true unless changed?
    return true if empty?

    self[:value] = encrypt(value) if private?

    true
  end

  private

  def sanitize(value)
    value.transform_values(&:to_f).to_h
  end

  def encrypt(value)
    value.transform_values do |v|
      super v
    end
  end

  def decrypt(value)
    value.transform_values do |v|
      super v
    end
  end
end
