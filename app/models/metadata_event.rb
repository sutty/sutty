# frozen_string_literal: true

# Gestiona eventos compatibles con jekyll-ical
class MetadataEvent < MetadataTemplate
  # Preferimos los husos horarios en números porque los husos con
  # nombres dejan afuera territorios que comparten el mismo huso que
  # otras ciudades más hegemónicas.
  #
  # @see {https://en.wikipedia.org/wiki/List_of_UTC_time_offsets}
  TIMEZONES = %w[-12:00 -11:00 -10:00 -09:30 -09:00 -08:00 -07:00 -06:00
                 -05:00 -04:00 -03:30 -03:00 -02:00 -01:00 00:00 +01:00 +02:00 +03:00
                 +03:30 +04:00 +04:30 +05:00 +05:30 +05:45 +06:00 +06:30 +07:00 +08:00
                 +08:45 +09:00 +09:30 +10:00 +10:30 +11:00 +12:00 +12:45 +13:00
                 +14:00].freeze

  # El valor por defecto es un Hash con algunas llaves pero queremos que
  # sea opcional.
  #
  # @return [Hash]
  def default_value
    super || {}
  end

  def to_param
    { name => {} }
  end

  # Dates are required and need to be parseable
  def validate
    self.errors = []
    times = []

    %w[dtstart dtend].each do |dt|
      errors << I18n.t("metadata.#{type}.zone_missing") unless TIMEZONES.include? value.dig(dt, 'zone')
      errors << I18n.t("metadata.#{type}.date_missing") if value.dig(dt, 'date').blank?

      begin
        Date.parse value.dig(dt, 'date')
      rescue ArgumentError, TypeError
        errors << I18n.t("metadata.#{type}.date_non_parseable")
      end

      unless (time = value.dig(dt, 'time')).blank?
        errors << I18n.t("metadata.#{type}.time_non_parseable") unless /[0-5][0-9]:[0-5][0-9]/ =~ time
      end

      times << value.dig(dt, 'date') + ' ' + value.dig(dt, 'time')
    end

    begin
      dtstart, dtend = times.map { |t| Time.parse t }
      errors << I18n.t("metadata.#{type}.end_in_the_past") if dtstart > dtend
    rescue ArgumentError
      errors << I18n.t("metadata.#{type}.time_non_parseable")
    end

    errors.empty?
  end

  private

  def sanitize(hash)
    %w[dtstart dtend].map do |dt|
      time = hash.dig(dt, 'time')

      [
        dt, {
          'zone' => hash.dig(dt, 'zone'),
          'date' => Date.parse(hash.dig(dt, 'date')).to_s,
          'time' => time.blank? ? '00:00' : time
        }
      ]
    end.to_h
  end
end
