# frozen_string_literal: true

# Nueva interfaz
class MetadataNewBelongsTo < MetadataBelongsTo
  include Metadata::UnusedValuesConcern
end
