# frozen_string_literal: true

# Almacena el UUID de otro Post y actualiza el valor en el Post
# relacionado.
class MetadataBelongsTo < MetadataRelatedPosts
  include Metadata::InverseConcern

  # TODO: Convertir algunos tipos de valores en módulos para poder
  # implementar varios tipos de campo sin repetir código
  #
  # @include MetadataString
  #
  # Una string vacía
  def default_value
    ''
  end

  # Obtiene el valor desde el documento.
  #
  # @return [String]
  def document_value
    document.data[name.to_s]
  end

  def indexable_values
    posts.find_by_post_uuid(value).try(:title)
  end

  alias to_s indexable_values

  private

  def sanitize(uuid)
    sanitize_string(uuid.to_s)
  end
end
