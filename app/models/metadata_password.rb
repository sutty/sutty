# frozen_string_literal: true

# Almacena una contraseña
class MetadataPassword < MetadataString
  # Las contraseñas no son indexables
  #
  # @return [boolean]
  def indexable?
    false
  end

  private

  # Sanitizar la string y generar un hash Bcrypt
  #
  # @param :string [String]
  # @return [String]
  def sanitize(value)
    ::BCrypt::Password.create(sanitize_string(value.to_s)).to_s
  end
end
