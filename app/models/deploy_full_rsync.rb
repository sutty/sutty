# frozen_string_literal: true

class DeployFullRsync < DeployRsync
  SOFT_DEPENDENCIES = %i[
    deploy_alternative_domain
    deploy_localized_domain
    deploy_hidden_service
    deploy_www
  ]

  # Sincroniza las ubicaciones alternativas también, ignorando las que
  # todavía no se generaron.  Solo falla si ningún sitio fue
  # sincronizado o si alguna sincronización falló.
  #
  # @param :output [Boolean]
  # @return [Boolean]
  def rsync(output: false)
    result =
      self.class.all_dependencies.map(&:to_s).map(&:classify).map do |dependency|
        site.deploys.where(type: dependency).find_each.map do |deploy|
          next unless File.exist? deploy.destination

          run %(rsync -aviH --delete-after --timeout=5 #{Shellwords.escape deploy.destination} #{Shellwords.escape destination}), output: output
        rescue StandardError
        end
      end.flatten.compact

    result.present? && result.all?
  end
end
