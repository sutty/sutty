# frozen_string_literal: true

# Los valores de este metadato son artículos en otros idiomas
class MetadataLocales < MetadataHasAndBelongsToMany
  # Todos los valores posibles para cada idioma disponible
  #
  # @return { lang: { title: uuid } }
  def values
    @values ||= other_locales.to_h do |other_locale|
      [
        other_locale,
        posts.where(locale: other_locale).pluck(:title, :layout, :post_id).to_h do |row|
          row.tap do |value|
            value[0] = "#{value[0]} (#{site.layouts[value.delete_at(1)].humanized_name})"
          end
        end
      ]
    end
  end

  # Siempre hay una relación inversa
  #
  # @return [True]
  def inverse?
    true
  end

  # El campo inverso se llama igual en el otro post
  #
  # @return [Symbol]
  def inverse
    :locales
  end

  private

  # Obtiene todos los locales distintos a este post
  #
  # @return [Array]
  def other_locales
    @other_locales ||= site.locales - [locale]
  end

  # Obtiene todos los posts de los otros locales con el mismo layout
  #
  # @return [IndexedPost::ActiveRecord_AssociationRelation]
  def posts
    site.indexed_posts.where(locale: other_locales).where(layout: post.layout.value).where.not(post_id: post.uuid.value)
  end
end
