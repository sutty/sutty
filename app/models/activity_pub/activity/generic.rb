# frozen_string_literal: true

class ActivityPub
  class Activity
    class Generic < ActivityPub::Activity; end
  end
end
