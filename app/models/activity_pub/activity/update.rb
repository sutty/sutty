# frozen_string_literal: true

class ActivityPub
  class Activity
    class Update < ActivityPub::Activity
      # Si estamos actualizando el objeto, tenemos que devolverlo a estado
      # de moderación
      def update_activity_pub_state!
        activity_pub.pause! if activity_pub.approved?
      end
    end
  end
end
