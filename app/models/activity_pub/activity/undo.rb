# frozen_string_literal: true

# = Undo =
#
# Deshace una actividad, dependiendo de la actividad a la que se
# refiere.
class ActivityPub
  class Activity
    class Undo < ActivityPub::Activity
      # Una actividad de deshacer tiene anidada como objeto la actividad
      # a deshacer.  Para respetar la voluntad de le actore remote,
      # tendríamos que eliminar cualquier actividad pendiente sobre el
      # objeto.
      #
      # Sin embargo, estas acciones nunca deberían llegar a nuestra
      # Inbox.
      #
      # @todo Validar que le Actor corresponda con los objetos. Esto ya
      # lo haría la Social Inbox por nosotres.
      # @see {https://github.com/hyphacoop/social.distributed.press/issues/43}
      def update_activity_pub_state!
        ActivityPub.transaction do
          ActivityPub::Activity.find_by(uri: content['object'])&.activity_pub&.remove!
          activity_pub.remove!
        end
      end
    end
  end
end
