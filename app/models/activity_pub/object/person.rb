# frozen_string_literal: true

# = Person =
#
# Una persona, el perfil de une actore
class ActivityPub
  class Object
    class Person < ActivityPub::Object
      include Concerns::ActorTypeConcern
    end
  end
end
