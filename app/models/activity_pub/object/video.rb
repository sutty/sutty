# frozen_string_literal: true

# = Video =
#
# Representa artículos
class ActivityPub
  class Object
    class Video < ActivityPub::Object; end
  end
end
