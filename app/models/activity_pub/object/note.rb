# frozen_string_literal: true

# = Note =
#
# Representa notas, el tipo más común de objeto del Fediverso.
class ActivityPub
  class Object
    class Note < ActivityPub::Object; end
  end
end
