# frozen_string_literal: true

# = Tombstone =
#
# Representa artículos
class ActivityPub
  class Object
    class Tombstone < ActivityPub::Object; end
  end
end
