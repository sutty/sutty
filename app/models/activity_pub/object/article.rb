# frozen_string_literal: true

# = Article =
#
# Representa artículos
class ActivityPub
  class Object
    class Article < ActivityPub::Object; end
  end
end
