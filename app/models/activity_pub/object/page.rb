# frozen_string_literal: true

# = Page =
#
# Representa artículos
class ActivityPub
  class Object
    class Page < ActivityPub::Object; end
  end
end
