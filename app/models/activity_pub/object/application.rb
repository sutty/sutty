# frozen_string_literal: true

# = Application =
#
# Una aplicación o instancia
class ActivityPub
  class Object
    class Application < ActivityPub::Object
      include Concerns::ActorTypeConcern
    end
  end
end
