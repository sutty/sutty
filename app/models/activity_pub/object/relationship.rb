# frozen_string_literal: true

# = Relationship =
#
# Representa artículos
class ActivityPub
  class Object
    class Relationship < ActivityPub::Object; end
  end
end
