# frozen_string_literal: true

# = Document =
#
# Representa artículos
class ActivityPub
  class Object
    class Document < ActivityPub::Object; end
  end
end
