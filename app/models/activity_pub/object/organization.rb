# frozen_string_literal: true

# = Organization =
#
# Una organización
class ActivityPub
  class Object
    class Organization < ActivityPub::Object
      include Concerns::ActorTypeConcern
    end
  end
end
