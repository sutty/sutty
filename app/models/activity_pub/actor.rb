# frozen_string_literal: true

# = Actor =
#
# Actor es la entidad que realiza acciones en ActivityPub
#
# @todo Obtener el perfil dinámicamente
class ActivityPub
  class Actor < ApplicationRecord
    include ActivityPub::Concerns::JsonLdConcern

    belongs_to :instance
    has_many :actor_moderation
    has_many :activity_pubs, as: :object
    has_many :activities
    has_many :remote_flags

    # Les actores son únicxs a toda la base de datos
    validates :uri, presence: true, url: true, uniqueness: true

    before_save :mentionize!

    # Obtiene el nombre de la Actor como mención, solo si obtuvimos el
    # contenido de antemano.
    #
    # @return [String, nil]
    def mentionize!
      return if mention.present?
      return if content['preferredUsername'].blank?
      return if instance.blank?

      self.mention ||= "@#{content['preferredUsername']}@#{instance.hostname}"
    end

    def object
      @object ||= ActivityPub::Object.lock.find_or_create_by(uri: uri)
    end

    def content
      object.content
    end
  end
end
