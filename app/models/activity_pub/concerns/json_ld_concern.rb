# frozen_string_literal: true

class ActivityPub
  module Concerns
    module JsonLdConcern
      extend ActiveSupport::Concern

      included do
        # Cuando asignamos contenido, obtener la URI si no lo hicimos ya
        before_save :uri_from_content!, unless: :uri?

        # Obtiene un tipo de actividad a partir del tipo informado
        #
        # @param object [Hash]
        # @return [Activity]
        def self.type_from(object)
          raise NameError unless object.is_a?(Hash)

          "#{model_name.name}::#{object[:type].presence || 'Generic'}".constantize
        rescue NameError
          model_name.name.constantize::Generic
        end

        private

        def uri_from_content!
          self.uri = content[:id]
        end
      end
    end
  end
end
