# frozen_string_literal: true

# Nueva interfaz
class MetadataNewPredefinedValue < MetadataPredefinedValue
  def values
    @values ||= (required ? {} : { I18n.t('posts.attributes.new_predefined_value.empty') => '' }).merge(super)
  end
end
