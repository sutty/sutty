# frozen_string_literal: true

# Un registro de cualquier cosa, nos sirve para debuguear
# selectivamente.
class LogEntry < ApplicationRecord
  belongs_to :site
  serialize :text, JSON

  default_scope -> { order(created_at: :desc) }

  def resend
    return if sent

    ContactJob.perform_later site, params[:form], params
  end

  def params
    @params ||= text&.dig('params')&.symbolize_keys
  end
end
