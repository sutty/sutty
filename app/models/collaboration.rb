# frozen_string_literal: true

# Una clase que contiene el sitio con el que se va a colaborar
Collaboration = Struct.new(:site)
