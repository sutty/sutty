# frozen_string_literal: true

# Define un campo de imagen
class MetadataImage < MetadataFile
  def validate
    super

    errors << I18n.t('metadata.image.not_an_image') if path? && !image?

    errors.compact!
    errors.empty?
  end

  # Determina si es una imagen
  def image?
    static_file&.blob&.send(:web_image?)
  end
end
