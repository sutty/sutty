# frozen_string_literal: true

# Recolecta estadísticas durante la generación del sitio
class BuildStat < ApplicationRecord
  belongs_to :deploy

  scope :jekyll, -> { where(action: 'bundle_exec_jekyll_build') }
end
