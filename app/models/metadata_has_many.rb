# frozen_string_literal: true

# La diferencia con MetadataRelatedPosts es que la relación también
# actualiza los Posts remotos.
#
# Localmente tenemos un Array de UUIDs.  Remotamente tenemos una String
# apuntando a un Post, que se mantiene actualizado como el actual.
class MetadataHasMany < MetadataRelatedPosts
  include Metadata::InverseConcern
end
