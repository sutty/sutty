# frozen_string_literal: true

# Define el rol que tiene una usuaria en un sitio
#
# Un rol puede ser temporal, es decir que aun no se ha aceptado y
# necesita del consentimiento de le usuarie :)
class Rol < ApplicationRecord
  ROLES = %w[usuarie invitade].freeze
  USUARIE = 'usuarie'
  INVITADE = 'invitade'

  belongs_to :usuarie
  belongs_to :site
  has_many :deploys

  validates_inclusion_of :rol, in: ROLES

  before_save :add_token_if_missing!

  def invitade?
    rol == INVITADE
  end

  def usuarie?
    rol == USUARIE
  end

  def self.role?(rol)
    ROLES.include? rol
  end

  private

  # Asegurarse que tenga un token
  def add_token_if_missing!
    self.token ||= SecureRandom.hex(64)
  end
end
