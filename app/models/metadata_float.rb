# frozen_string_literal: true

# Un campo numérico de punto flotante
class MetadataFloat < MetadataTemplate
  # Nada
  def default_value
    super || nil
  end

  def save
    return true unless changed?

    self[:value] = encrypt(value) if private?

    true
  end

  # Indicarle al navegador que acepte números decimales
  #
  # @return [Float]
  def step
    0.05
  end

  private

  def sanitize(value)
    if value.respond_to?(:to_f)
      value.to_f
    else
      default_value
    end
  end

  def decrypt(value)
    super(value).to_f
  end
end
