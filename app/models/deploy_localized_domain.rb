# frozen_string_literal: true

# Soportar dominios localizados
class DeployLocalizedDomain < DeployAlternativeDomain
  store_accessor :values, :hostname
  store_accessor :values, :locale

  # Generar un link simbólico del sitio principal al alternativo
  def deploy(**)
    File.symlink?(destination) ||
      File.symlink(File.join(site.hostname, locale), destination).zero?
  end
end
