# frozen_string_literal: true

module Metadata
  module InverseConcern
    extend ActiveSupport::Concern

    included do
      # Hay una relación inversa?
      #
      # @return [Boolean]
      def inverse?
        inverse.present?
      end

      # La relación inversa
      #
      # @return [Nil,Symbol]
      def inverse
        @inverse ||= layout.metadata.dig(name, 'inverse')&.to_sym
      end
    end
  end
end
