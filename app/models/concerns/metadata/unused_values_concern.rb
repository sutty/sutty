# frozen_string_literal: true

module Metadata
  # Hasta ahora veníamos habilitando la opción de romper
  # retroactivamente relaciones, sin informar que estaba sucediendo.
  # Con este módulo, todas las relaciones que ya tienen una relación
  # inversa son ignoradas.
  module UnusedValuesConcern
    extend ActiveSupport::Concern

    included do
      # Excluye el Post actual y todos los que ya tengan una relación
      # inversa, para no romperla.
      #
      # @return [Array]
      def values
        @values ||= posts.map do |p|
          next if p.post_id == post.uuid.value

          disabled = false

          # El campo está deshabilitado si está completo y no incluye el
          # post actual.
          if inverse?
            disabled = p[inverse].present? && ![p[inverse].value].flatten.include?(post.uuid.value)
          end

          [p.title, p.post_id, disabled]
        end.compact
      end
    end
  end
end
