# frozen_string_literal: true

# Un tipo de datos oculto
class MetadataHidden < MetadataString; end
