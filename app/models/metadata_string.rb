# frozen_string_literal: true

# Un campo de texto
class MetadataString < MetadataTemplate
  # Una string vacía
  def default_value
    super || ''
  end

  def indexable?
    true && !private?
  end

  def titleize?
    true
  end

  private

  # No se permite HTML en las strings
  #
  # @param string [any]
  # @return [String]
  def sanitize(string)
    return '' if string.to_s.blank?

    sanitize_string(string.to_s)
  end
end
