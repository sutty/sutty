# frozen_string_literal: true

# Representa la plantilla de un campo en los metadatos del artículo
#
# TODO: Validar el tipo de valor pasado a value= según el :type
#
MetadataTemplate = Struct.new(:site, :document, :name, :label, :type,
                              :value, :help, :required, :errors, :post,
                              :layout, keyword_init: true) do
  # Determina si el campo es indexable
  def indexable?
    false
  end

  def nested?
    false
  end

  # El valor puede ser parte de un título auto-generado
  def titleize?
    false
  end

  def inspect
    "#<#{self.class} site=#{site.name.inspect} post=#{post.id.inspect} value=#{value.inspect}>"
  end

  # Queremos que los artículos nuevos siempre cacheen, si usamos el UUID
  # siempre vamos a obtener un item nuevo.
  def cache_key
    return "#{layout.value}/#{name}" if post.new?

    @cache_key ||= "post/#{post.uuid.value}/#{name}"
  end

  # Genera una versión de caché en base a la fecha de modificación del
  # Post, el valor actual y los valores posibles, de forma que cualquier
  # cambio permita renovar la caché.
  #
  # @return [String]
  def cache_version
    post.cache_version + value.hash.to_s + values.hash.to_s
  end

  # @return [String]
  def cache_key_with_version
    "#{cache_key}-#{cache_version}"
  end

  # Siempre obtener el valor actual y solo obtenerlo del documento una
  # vez.
  #
  # TODO: Repensar si queremos el valor anterior o el original
  def value_was
    @value_was ||= document_value.nil? ? default_value : document_value
  end

  # El valor cambió si usamos #value= y el valor es distinto que en el
  # documento.
  def changed?
    return false unless instance_variable_defined?(:@value_was)

    value_was != value
  end

  # Obtiene el valor del JekyllDocument
  def document_value
    document.data[name.to_s]
  end

  # Trae el idioma actual del sitio o del panel
  #
  # @deprecated Empezar a usar locale
  # @return [String]
  def lang
    @lang ||= post&.lang&.value || I18n.locale.to_s
  end

  alias_method :locale, :lang

  # El valor por defecto desde el esquema de datos
  #
  # @return [any]
  def default_value
    layout.metadata.dig(name, 'default', lang)
  end

  # Valores posibles, busca todos los valores actuales en otros
  # artículos del mismo sitio
  #
  # @return [Array]
  def values
    site.indexed_posts.everything_of(name)
  end

  # Modificar el valor actual o no hacer nada si son iguales.  Queremos
  # poder vaciar valores y detectar cuándo pasamos de nil a un valor.
  #
  # @param new_value [any]
  # @return [any]
  def value=(new_value)
    # Si el valor es nulo, no sanitizar
    new_value = sanitize(new_value) unless new_value.nil?

    @niled = new_value.nil?
    # Guardar el valor anterior
    @value_was = self[:value]

    self[:value] = new_value

    # El post es modificado si cambió con respecto a lo que está
    # guardado, los valores intermedios no nos interesan.
    post.modified! if changed?

    # Devolver el valor actual
    self[:value]
  end

  # Valor actual o por defecto.  Al memoizarlo podemos modificarlo
  # usando otros métodos que el de asignación.
  #
  # Activamos la asignación de valores por defecto
  def value
    self[:value] ||=
      unless @niled
        self.value =
          if (data = document_value).present?
            if private?
              decrypt(data)
            else
              data
            end
          else
            default_value
          end
      end
  end

  # Detecta si el valor está vacío
  def empty?
    value.blank?
  end

  # Comprueba si el metadato es válido
  def valid?
    validate
  end

  def validate
    self.errors = []

    errors << I18n.t('metadata.cant_be_empty') unless can_be_empty?

    errors.empty?
  end

  # Usa el valor por defecto para generar el formato de StrongParam
  # necesario.
  #
  # @return [Symbol,Hash]
  def to_param
    case default_value
    when Hash then { name => default_value.keys.map(&:to_sym) }
    when Array then { name => [] }
    else name
    end
  end

  def to_s
    value.to_s
  end

  # Decide si el metadato se coloca en el front_matter o no
  def front_matter?
    true
  end

  def array?
    type == 'array'
  end

  # En caso de que algún campo necesite realizar acciones antes de ser
  # guardado
  def save
    # Mantener el valor cifrado si el campo era privado
    self[:value] = document_value if private? && !changed?

    return true unless changed?

    # Cifrar si cambió
    self[:value] = encrypt(value) if private?

    true
  end

  def related_posts?
    false
  end

  def related_methods
    @related_methods ||= [].freeze
  end

  # Determina si el campo es privado y debería ser cifrado
  def private?
    layout.metadata.dig(name, 'private').present?
  end

  # Determina si el campo debería estar deshabilitado
  def disabled?
    layout.metadata.dig(name, 'disabled') || !writable?
  end

  # Determina si el campo es de solo lectura
  #
  # once => el campo solo se puede modificar si estaba vacío
  def writable?
    case layout.metadata.dig(name, 'writable')
    when 'once' then value_was.blank?
    else true
    end
  end

  private

  # Si es obligatorio no puede estar vacío
  def can_be_empty?
    true unless required && empty?
  end

  # No usamos sanitize_action_text_content porque espera un ActionText
  #
  # Ver ActionText::ContentHelper#sanitize_action_text_content
  def sanitize(_)
    raise NotImplementedError, "#{type} doesn't implement a sanitize method"
  end

  # Elimina HTML de las strings, opcionalmente manteniendo algunos
  # elementos y atributos.
  #
  # @param string [any]
  # @param :tags [Array<String>]
  # @param :attributes [Array<String>]
  # @return [String]
  def sanitize_string(string, tags: [], attributes: [])
    sanitizer = Rails::Html::Sanitizer.safe_list_sanitizer.new

    sanitizer.sanitize(string.to_s.tr("\r", '').unicode_normalize, tags:, attributes:).strip.html_safe
  end

  # Decifra el valor
  #
  # XXX: Otros tipos de valores necesitan implementar su propio método
  # de decifrado (Array).
  def decrypt(value)
    return value if value.blank?

    box.decrypt_str value.to_s
  rescue Lockbox::DecryptionError => e
    if value.to_s.include? ' '
      value
    else
      ExceptionNotifier.notify_exception(e, data: { site: site.name, post: post.path.absolute, name: name })

      I18n.t('lockbox.help.decryption_error')
    end
  end

  # Cifra el valor.
  #
  # XXX: Otros tipos de valores necesitan implementar su propio método
  # de cifrado (Array).
  def encrypt(value)
    box.encrypt value.to_s
  end

  # Genera una lockbox a partir de la llave privada del sitio
  #
  # @return [Lockbox]
  def box
    @box ||= Lockbox.new key: site.private_key, padding: true, encode: true
  end
end
