# frozen_string_literal: true

# Una plantilla agrupa metadatos que va a tener un artículo.
#
# TODO: Renombrar metadatos a atributos o campos, ahora se llaman de
# varias formas por todo el código.
Layout = Struct.new(:site, :name, :meta, :metadata, keyword_init: true) do
  def value
    name.to_s
  end

  # Obtiene todos los layouts (schemas) dependientes de este.
  #
  # @return [Array]
  def schemas
    @schemas ||= site.layouts.to_h.slice(*site.schema_organization[name]).values
  end

  def attributes
    @attributes ||= metadata.keys.map(&:to_sym)
  end

  # Busca la traducción del Layout en el sitio o intenta humanizarlo
  # según Rails.
  #
  # @return [String]
  def humanized_name
    @humanized_name ||= site.i18n.dig('layouts', name.to_s) || name.to_s.humanize
  end

  # Detecta si el Layout no debería mostrarse
  #
  # @return [Boolean]
  def hidden?
    meta[:hidden].present?
  end

  # Detecta si el layout no es renderizable
  def ignored?
    @ignored ||= site.config['ignored_layouts']&.include? name.to_s
  end

  # Los metadatos del Layout.
  #
  # TODO: Inicializar con valores por defecto o usar Hash#default_proc
  #
  # @return [Hash]
  def meta
    @meta ||= self[:meta] || {}
  end
end
