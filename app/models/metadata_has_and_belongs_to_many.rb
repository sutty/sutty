# frozen_string_literal: true

# Establece una relación de muchos a muchos artículos
class MetadataHasAndBelongsToMany < MetadataHasMany
end
