# frozen_string_literal: true

class MetadataDate < MetadataTemplate
  # La fecha de hoy si no hay nada.  Podemos traer un valor por defecto
  # desde el esquema, siempre y cuando pueda considerarse una fecha
  # válida.
  #
  # @return [Date,nil]
  def default_value
    if (dv = super.presence)
      begin
        Date.parse(dv)
      # XXX: Notificar para que sepamos que el esquema no es válido.
      # TODO: Validar el valor por defecto en sutty-schema-validator.
      rescue Date::Error => e
        ExceptionNotifier.notify_exception(e, data: { site: site.name, post: post.id, name:, type: })
        nil
      end
    end
  end

  # Delegar el formato al valor, para uso dentro de date_field()
  #
  # @param format [String]
  # @return [String,nil]
  def strftime(format)
    value&.strftime(format)
  end

  private

  def sanitize(value)
    case value
    when Time then value
    when Date then value.to_time
    when NilClass then (document_value || default_value)
    else Date.parse(value.to_s)
    end
  rescue ArgumentError, TypeError
    default_value
  end
end
