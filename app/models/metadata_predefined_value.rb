# frozen_string_literal: true

# Un campo de texto seleccionado de una lista de valores posibles
class MetadataPredefinedValue < MetadataString
  # Obtiene todos los valores desde el layout, en un formato compatible
  # con options_for_select.
  #
  # @return [Hash]
  def values
    @values ||= layout.dig(:metadata, name, 'values', I18n.locale.to_s)&.invert || {}
  end

  def to_s
    values.invert[value].to_s
  end

  private

  # Solo permite almacenar los valores predefinidos.
  #
  # @return [String]
  def sanitize(string)
    v = sanitize_string string

    return '' unless values.values.include? v

    v
  end
end
