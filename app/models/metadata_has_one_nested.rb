# frozen_string_literal: true

class MetadataHasOneNested < MetadataHasOne
  def nested
    @nested ||= layout.metadata.dig(name, 'nested')
  end

  def nested?
    true
  end

  # No tener conflictos con related
  def related_methods
    @related_methods ||= [].freeze
  end
end
