# frozen_string_literal: true

# Políticas de privacidad
class PrivacyPolicy < ApplicationRecord
  extend Mobility

  translates :title, type: :string, locale_accessors: true
  translates :description, type: :text, locale_accessors: true
  translates :content, type: :text, locale_accessors: true

  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
  validates :content, presence: true
end
