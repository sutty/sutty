# frozen_string_literal: true

# Soportar dominios alternativos
class DeployAlternativeDomain < Deploy
  store_accessor :values, :hostname

  DEPENDENCIES = %i[deploy_local]

  # Generar un link simbólico del sitio principal al alternativo
  def deploy(**)
    File.symlink?(destination) ||
      File.symlink(site.hostname, destination).zero?
  end

  # No hay límite para los dominios alternativos
  def limit; end

  def size
    File.size destination
  end

  def destination
    @destination ||= File.join(Rails.root, '_deploy', fqdn)
  end

  def fqdn
    hostname.gsub(/\.\z/, '')
  end

  def url
    "https://#{File.basename destination}"
  end
end
