# frozen_string_literal: true

# Alojamiento local, solo genera el sitio, con lo que no necesita hacer
# nada más
class DeployLocal < Deploy
  before_destroy :remove_destination!

  def bundle(output: false)
    run %(bundle config set --local clean 'true'), output: output
    run(%(bundle config set --local deployment 'true'), output: output) if site.gemfile_lock_path?
    run %(bundle config set --local path '#{gems_dir}'), output: output
    run %(bundle config set --local without 'test development'), output: output
    run %(bundle config set --local cache_all 'false'), output: output
    run %(bundle install), output: output
  end

  def git_lfs(output: false)
    run %(git lfs fetch), output: output
    run %(git lfs checkout), output: output
  end

  # Realizamos la construcción del sitio usando Jekyll y un entorno
  # limpio para no pasarle secretos
  #
  # Pasamos variables de entorno mínimas para no filtrar secretos de
  # Sutty
  def deploy(output: false)
    return false unless mkdir
    return false unless git_lfs(output: output)
    return false unless yarn(output: output)
    return false unless pnpm(output: output)
    return false unless bundle(output: output)

    jekyll_build(output: output)
  end

  # Sólo permitimos un deploy local
  def limit
    1
  end

  def url
    site.url
  end

  # Obtener el tamaño de todos los archivos y directorios (los
  # directorios son archivos :)
  def size
    @size ||= begin
      paths = [destination, File.join(destination, '**', '**')]

      Dir.glob(paths).map do |file|
        if File.symlink? file
          0
        else
          File.size(file)
        end
      end.inject(:+)
    end
  end

  def destination
    File.join(Rails.root, '_deploy', site.hostname)
  end

  # Libera espacio eliminando archivos temporales
  #
  # @return [nil]
  def cleanup!
    FileUtils.rm_rf(site.bundle_path)
    FileUtils.rm_rf(yarn_cache_dir)
    FileUtils.rm_rf(File.join(site.path, 'node_modules'))
    FileUtils.rm_rf(File.join(site.path, '.sass-cache'))
    FileUtils.rm_rf(File.join(site.path, '.jekyll-cache'))
  end

  # Opciones necesarias para la compilación del sitio
  #
  # @return [Hash]
  def local_env
    @local_env ||= {
      'SPREE_API_KEY' => site.tienda_api_key,
      'SPREE_URL' => site.tienda_url,
      'AIRBRAKE_PROJECT_ID' => site.id.to_s,
      'AIRBRAKE_PROJECT_KEY' => site.airbrake_api_key,
      'YARN_CACHE_FOLDER' => yarn_cache_dir,
      'GEMS_SOURCE' => ENV['GEMS_SOURCE']
    }
  end

  private

  def mkdir
    FileUtils.mkdir_p destination
  end

  def yarn_cache_dir
    Rails.root.join('_yarn_cache').to_s
  end

  def pnpm_cache_dir
    Rails.root.join('_pnpm_cache').to_s
  end

  def yarn_lock
    File.join(site.path, 'yarn.lock')
  end

  def yarn_lock?
    File.exist? yarn_lock
  end

  def pnpm_lock
    File.join(site.path, 'pnpm-lock.yaml')
  end

  def pnpm_lock?
    File.exist? pnpm_lock
  end

  def pnpm(output: false)
    return true unless pnpm_lock?

    run %(pnpm config set store-dir "#{pnpm_cache_dir}"), output: output
    run 'pnpm install --production', output: output
  end

  def gem(output: false)
    run %(gem install bundler --no-document), output: output
  end

  # Corre yarn dentro del repositorio
  def yarn(output: false)
    return true unless yarn_lock?

    run 'yarn install --production', output: output
  end

  def jekyll_build(output: false)
    with_tempfile(site.private_key_pem) do |file|
      flags = extra_flags(private_key: file)

      run %(bundle exec jekyll build --trace --profile #{flags} --destination "#{escaped_destination}"), output: output
    end
  end

  # no debería haber espacios ni caracteres especiales, pero por si
  # acaso...
  def escaped_destination
    Shellwords.escape destination
  end

  # Eliminar el destino si se elimina el deploy
  def remove_destination!
    FileUtils.rm_rf destination
  end

  # Genera opciones extra desde los otros deploys
  #
  # @param :args [Hash]
  # @return [String]
  def extra_flags(**args)
    site.deployment_list.map do |deploy|
      deploy.flags_for_build(**args)
    end.compact.join(' ')
  end
end
