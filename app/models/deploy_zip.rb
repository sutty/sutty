# frozen_string_literal: true

require 'zip'

# Genera un ZIP a partir del sitio ya construido
#
# TODO: Firmar con minisign
class DeployZip < Deploy
  DEPENDENCIES = %i[deploy_local]

  # Una vez que el sitio está generado, tomar todos los archivos y
  # y generar un zip accesible públicamente.
  #
  # rubocop:disable Metrics/MethodLength
  def deploy(output: false)
    FileUtils.rm_f path
    time_start
    Zip::File.open(path, Zip::File::CREATE) do |zip|
      Dir.glob(File.join(destination, '**', '**')).each do |file|
        entry = Pathname.new(file).relative_path_from(destination).to_s

        if File.directory? file
          log "Creando directorio #{entry}", output

          zip.mkdir(entry)
        else
          log "Comprimiendo #{entry}", output
          zip.add(entry, file)
        end
      end
    end

    time_stop

    File.exist?(path).tap do |status|
      build_stats.create action: 'zip',
                        seconds: time_spent_in_seconds,
                        bytes: size,
                        log: @log.join("\n"),
                        status: status
    end
  rescue Zip::Error => e
    ExceptionNotifier.notify_exception(e, data: { site: site.name })

    build_stats.create action: 'zip',
                       seconds: 0,
                       bytes: 0,
                       log: @log.join("\n"),
                       status: false

    false
  end
  # rubocop:enable Metrics/MethodLength

  def limit
    1
  end

  def size
    File.size path
  end

  # @return [String]
  def destination
    Rails.root.join('_deploy', site.hostname).realpath.to_s
  rescue Errno::ENOENT
    Rails.root.join('_deploy', site.hostname).to_s
  end

  def file
    "#{site.hostname}.zip"
  end

  def url
    "#{site.url}#{file}"
  end

  def path
    File.join(destination, file)
  end

  private

  # @param :line [String]
  # @param :output [Boolean]
  def log(line, output)
    @log ||= []
    @log  << line

    puts line if output
  end
end
