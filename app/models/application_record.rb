# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Obtener una lista filtrada de atributos al momento de serializar
  #
  # @return [String]
  def to_yaml(options = {})
    pruned_attributes.to_yaml(options)
  end

  # Devuelve todos los atributos menos los filtrados
  #
  # @return [Hash]
  def pruned_attributes
    self.class.inspection_filter.filter(serializable_hash)
  end

  # @param coder [Psych::Coder]
  # @return nil
  def encode_with(coder)
    pruned_attributes.each_pair do |attr, value|
      coder[attr] = value
    end

    nil
  end
end
