# frozen_string_literal: true

# Un campo numérico
class MetadataNumber < MetadataTemplate
  # Nada
  def default_value
    super.presence || nil
  end

  def save
    return true unless changed?

    self[:value] = encrypt(value) if private?

    true
  end

  private

  def sanitize(value)
    if value.respond_to?(:to_i)
      value.to_i
    else
      default_value
    end
  end

  def decrypt(value)
    super(value).to_i
  end
end
