# frozen_string_literal: true

# Devuelve una lista de títulos y UUID de todos los posts del mismo
# idioma que el actual, para usar con input-map.js
class MetadataRelatedPosts < MetadataArray
  # Genera un Hash de { title (schema) => uuid } para usar en
  # options_for_select
  #
  # @return [Hash]
  def values
    @values ||= posts.pluck(:title, :created_at, :layout, :post_id).to_h do |row|
      row.tap do |value|
        value[0] = "#{value[0]} #{value.delete_at(1).strftime('%F')} (#{site.layouts[value.delete_at(1)].humanized_name})"
      end
    end
  end

  # Las relaciones nunca son privadas
  def private?
    false
  end

  def indexable?
    false
  end

  def titleize?
    false
  end

  def indexable_values
    posts.where(post_id: value).pluck(:title)
  end

  # Encuentra el filtro
  #
  # @return [Hash]
  def filter
    layout.metadata.dig(name, 'filter')&.to_h&.symbolize_keys
  end

  private

  # Obtiene todos los posts menos el actual y opcionalmente los filtra
  #
  # @return [IndexedPost::ActiveRecord_AssociationRelation]
  def posts
    site.indexed_posts.where(locale: locale).where.not(post_id: post.uuid.value).where(filter)
  end

  # XXX: Mientras sean strings, no nos importa el formato
  #
  # @return [any]
  # @return [Array<String>]
  def sanitize(uuids)
    [uuids].flatten.compact.reject(&:blank?).map do |uuid|
      sanitize_string uuid.to_s
    end.reject(&:blank?).uniq
  end
end
