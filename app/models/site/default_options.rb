# frozen_string_literal: true

require 'dry-schema'

class Site
  # Las opciones por defecto se aplican durante la creación del sitio y
  # luego se permite a les usuaries modificarlas según quieran.  Por el
  # momento las opciones nuevas que aparezcan no modifican un sitio que
  # ya existe.
  module DefaultOptions
    extend ActiveSupport::Concern

    Schema = Dry::Schema.Params do
      optional(:colaboracion_anonima).value(:bool)
      optional(:contact).value(:bool)
      optional(:acepta_invitades).value(:bool)
      optional(:slugify_mode).value(included_in?: Jekyll::Utils::SLUGIFY_MODES)
      optional(:pagination).value(:bool)
    end

    included do
      validate :validate_options_from_theme!, if: :persisted?

      # @return [Dry::Schema::Result]
      def options_from_theme
        @options_from_theme ||= Schema.call(data['sutty'])
      end

      def update_options_from_theme
        return true if options_from_theme.to_h.blank?

        update(**options_from_theme.to_h)
      end

      private

      def validate_options_from_theme!
        options_from_theme.errors.each do |error|
          errors.add(:default_options, "#{error.path.map(&:to_s).join('/')} #{error} (#{error.input})")
        end
      end
    end
  end
end
