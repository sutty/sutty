# frozen_string_literal: true

class Site
  # Representa la configuración del sitio de forma que podamos leer y
  # escribir en el archivo _config.yml
  class Config < OpenStruct
    def initialize(site)
      # Iniciar el OpenStruct con el sitio
      super(site: site)

      @saved = File.exist? path

      read if @saved
    end

    # Obtener un valor por defecto a partir de la configuración
    def fetch(key, default)
      send(:[], key) || default
    end

    # Leer el archivo de configuración y setear los atributos en el
    # objeto actual, creando los metodos de ostruct
    def read
      data = YAML.safe_load(File.read(path), permitted_classes: [Time])
      @hash = data.hash

      data.each do |key, value|
        send("#{key}=".to_sym, value)
      end
    end

    # Escribe los cambios en el repositorio
    def write
      return true if persisted?

      @saved = Site::Writer.new(site: site, file: path, content: content.to_yaml).save.tap do |result|
        # Actualizar el hash para no escribir dos veces
        @hash = content.hash
      end
    end
    alias save write

    # Detecta si la configuración cambió comparando con el valor inicial
    def persisted?
      (@hash == content.hash) && @saved
    end

    # Obtener el contenido de la configuración como un hash, sin el
    # sitio correspondiente.
    def content
      h = to_h.stringify_keys
      h.delete 'site'

      h
    end

    # Obtener la ruta donde se encuentra la configuración.
    def path
      File.join site.path, '_config.yml'
    end
  end
end
