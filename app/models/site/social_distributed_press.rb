# frozen_string_literal: true

require 'distributed_press/v1/social/client'

class Site
  # Agrega soporte para Social Distributed Press en los sitios
  module SocialDistributedPress
    extend ActiveSupport::Concern

    included do
      has_encrypted :private_key_pem

      has_many :activity_pubs
      has_many :instance_moderations
      has_many :actor_moderations
      has_many :fediblock_states
      has_many :instances, through: :instance_moderations
      has_many :remote_flags, class_name: 'ActivityPub::RemoteFlag'

      before_save :generate_private_key_pem!, unless: :private_key_pem?

      def moderation_enabled?
        deploy_social_inbox.present?
      end

      def deploy_social_inbox
        @deploy_social_inbox ||= deploys.find_by(type: 'DeploySocialDistributedPress')
      end

      def moderation_checked!
        deploy_social_inbox.touch
      end

      # @return [Bool]
      def moderation_needed?
        return false unless moderation_enabled?

        last_activity_pub = activity_pubs.order(updated_at: :desc).first&.updated_at

        return false if last_activity_pub.blank?

        last_activity_pub > deploy_social_inbox.updated_at
      end

      # @return [SocialInbox]
      def social_inbox
        @social_inbox ||= SocialInbox.new(site: self)
      end

      # Obtiene el hostname de la API de Sutty
      #
      # @return [String]
      def social_inbox_hostname
        Rails.application.routes.default_url_options[:host].sub('panel', 'api')
      end

      private

      # Genera la llave privada y la almacena
      #
      # @return [nil]
      def generate_private_key_pem!
        self.private_key_pem ||= DistributedPress::V1::Social::Client.new(
          public_key_url: nil,
          key_size: 2048
        ).private_key.export
      end
    end
  end
end
