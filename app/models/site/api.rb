# frozen_string_literal: true

class Site
  module Api
    extend ActiveSupport::Concern

    included do
      has_encrypted :api_key
      before_save :add_api_key_if_missing!

      # Genera mensajes secretos que podemos usar para la API de cada
      # sitio.
      #
      # XXX: Si no se configura una API key del sitio o genérica, no
      # tenemos forma de verificar los mensajes, pero la generación de
      # llaves no va a fallar.
      def verifier
        @verifier ||= ActiveSupport::MessageVerifier.new(api_key || Rails.application.credentials.api_key || SecureRandom.hex(64))
      end

      def airbrake_api_key
        @airbrake_api_key ||= verifier.generate(airbrake_secret, purpose: :airbrake)
      end

      def airbrake_valid?(token)
        ActiveSupport::SecurityUtils.secure_compare(verifier.verify(token, purpose: :airbrake), airbrake_secret)
      rescue ActiveSupport::MessageVerifier::InvalidSignature
        false
      end

      private

      def airbrake_secret
        Rails.application.credentials.airbrake || SecureRandom.hex(64)
      end

      # Asegurarse que el sitio tenga una llave para la API
      def add_api_key_if_missing!
        self.api_key ||= SecureRandom.hex(64)
      end
    end
  end
end
