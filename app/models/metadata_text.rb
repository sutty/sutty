# frozen_string_literal: true

# Un campo de texto largo
class MetadataText < MetadataString
  def titleize?
    false
  end
end
