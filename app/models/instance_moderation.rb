# frozen_string_literal: true

# Mantiene el registro de relaciones entre sitios e instancias
class InstanceModeration < ApplicationRecord
  IGNORED_EVENTS = [].freeze
  IGNORED_STATES = [].freeze

  include AASM

  belongs_to :site
  belongs_to :instance, class_name: 'ActivityPub::Instance'

  aasm do
    state :paused, initial: true
    state :allowed
    state :blocked

    error_on_all_events do |e|
      ExceptionNotifier.notify_exception(e,
                                         data: { site: site.name, instance: instance.hostname,
                                                 instance_moderation: id })
    end

    after_all_events do
      ActivityPub::SyncListsJob.perform_later(site: site)
    end

    # Al volver la instancia a pausa no cambiamos el estado de
    # moderación de actores pre-existente.
    event :pause do
      transitions from: %i[allowed blocked], to: :paused
    end

    # Al permitir, solo bloqueamos la instancia, sin modificar el estado
    # de les actores y comentarios retroactivamente.
    event :allow do
      transitions from: %i[paused blocked], to: :allowed
    end

    # Al bloquear, solo bloqueamos la instancia, sin modificar el estado
    # de les actores y comentarios retroactivamente.
    event :block do
      transitions from: %i[paused allowed], to: :blocked
    end
  end

  # Definir eventos en masa
  include AasmEventsConcern
end
