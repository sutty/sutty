# frozen_string_literal: true


module AutoPublishConcern 
  extend ActiveSupport::Concern

  included do 
    def auto_publish!
      DeployJob.perform_later(site) if site.auto_publish?
    end
  end
end
