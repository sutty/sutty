# frozen_string_literal: true

module ActiveStorage
  module Attached::Changes::CreateOneDecorator
    extend ActiveSupport::Concern

    included do
      private

      # A partir de ahora todos los archivos se suben al servicio de
      # cada sitio.
      def attachment_service_name
        record.name.to_sym
      end
    end
  end
end

ActiveStorage::Attached::Changes::CreateOne.include ActiveStorage::Attached::Changes::CreateOneDecorator
