# frozen_string_literal: true

module CoreExtensions
  module String
    # Elimina el HTML
    module StripTags
      def strip_tags
        ActionController::Base.helpers.strip_tags(self)
      end
    end
  end
end
