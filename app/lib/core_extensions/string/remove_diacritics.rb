# frozen_string_literal: true

module CoreExtensions
  module String
    # Elimina tildes
    module RemoveDiacritics
      def remove_diacritics
        unicode_normalize(:nfd).gsub(/[^\x00-\x7F]/, '')
      end
    end
  end
end
