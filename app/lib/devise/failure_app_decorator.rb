# frozen_string_literal: true

module Devise
  module FailureAppDecorator
    extend ActiveSupport::Concern

    included do
      include AbstractController::Callbacks

      around_action :set_locale

      private

      def set_locale(&action)
        I18n.with_locale(session[:locale] || I18n.locale, &action)
      end
    end
  end
end

Devise::FailureApp.include Devise::FailureAppDecorator
