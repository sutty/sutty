# frozen_string_literal: true

# Valida URLs
#
# @see {https://storck.io/posts/better-http-url-validation-in-ruby-on-rails/}
class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.blank?
      record.errors.add(attribute, :url_missing)
      return
    end

    uri = URI.parse(value)

    record.errors.add(attribute, :scheme_missing) if uri.scheme.blank?
    record.errors.add(attribute, :host_missing) if uri.host.blank?
  rescue URI::Error
    record.errors.add(attribute, :invalid)
  end
end
