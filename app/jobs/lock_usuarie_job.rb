# frozen_string_literal: true

# Bloquea el acceso a une usuarie
class LockUsuarieJob < ApplicationJob
  # Cambiamos la contraseña, aplicamos un bloqueo y cerramos la sesión
  # para que no pueda volver a entrar hasta que siga las instrucciones
  # de desbloqueo.
  #
  # @param :usuarie [Usuarie]
  # @return [nil]
  def perform(usuarie:)
    password = SecureRandom.base36

    usuarie.skip_password_change_notification!
    usuarie.update(password: password, password_confirmation: password, remember_created_at: nil, locked_at: Time.now.utc)

    nil
  end
end
