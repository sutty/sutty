# frozen_string_literal: true

# Envía los mensajes de mantenimiento
#
# TODO: Interfaz web desde el panel de gestión.  Mientras tanto:
#
# docker exec -it sutty /bin/sh
# su app
# cd
# bundle exec rails c
# m = Maintenance.create message_en: 'reason', message_es: 'razón',
#   estimated_from: Time.now, estimated_to: Time.now + 1.hour
# MaintenanceJob.perform_later(maintenance_id: m.id)
#
# Lo mismo para salir de mantenimiento, agregando el atributo
# are_we_back: true al crear el Maintenance.
class MaintenanceJob < ApplicationJob
  def perform(maintenance:)
    # Decidir cuál vamos a enviar según el estado de Maintenance
    mailer = maintenance.are_we_back ? :were_back : :notice

    # XXX: Parece que [0] es más rápido que []#first
    Usuarie.all.pluck(:email, :lang).each do |u|
      MaintenanceMailer.with(maintenance: maintenance,
                             email: u[0],
                             lang: u[1]).public_send(mailer).deliver_now
    rescue Net::SMTPServerBusy => e
      # Algunas direcciones no son válidas, no queremos detener el
      # envío pero sí enterarnos cuáles son
      ExceptionNotifier.notify_exception(e, data: { maintenance_id: maintenance_id, email: u[0] })
    end
  end
end
