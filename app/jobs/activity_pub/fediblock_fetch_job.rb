# frozen_string_literal: true

class ActivityPub
  # Se encarga de mantener las listas de bloqueo actualizadas.  Luego de
  # actualizar el listado de instancias, bloquea las instancias en cada
  # sitio que tenga el fediblock habilitado.
  class FediblockFetchJob < ApplicationJob
    self.priority = 50

    def perform
      ActivityPub::Fediblock.find_each do |fediblock|
        fediblock.process!

        hostnames_added = fediblock.hostnames - fediblock.hostnames_was

        # No hacer nada si no cambió con respecto a la versión anterior
        next if hostnames_added.empty?

        ActivityPub::FediblockUpdatedJob.perform_later(fediblock: fediblock, hostnames: hostnames_added)
      rescue ActivityPub::Fediblock::FediblockDownloadError => e
        ExceptionNotifier.notify_exception(e, data: { fediblock: fediblock.title })
      end
    end
  end
end
