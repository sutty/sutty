# frozen_string_literal: true

class ActivityPub
  class InboxJob < ApplicationJob
    self.priority = 10

    # @param :site [Site]
    # @param :activity [String]
    # @param :action [Symbol]
    def perform(site:, activity:, action:)
      response = site.social_inbox.inbox.public_send(action, id: activity)

      raise response.body unless response.success?
    end
  end
end
