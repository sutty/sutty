# frozen_string_literal: true

# Se encarga de mantener sincronizadas las listas de instancias
# de los fediblocks con los sitios que las tengan activadas.
#
# También va a asociar las listas con todos los sitios que tengan la
# Social Inbox habilitada.
class ActivityPub
  class FediblockUpdatedJob < ApplicationJob
    self.priority = 50

    # @param :fediblock [ActivityPub::Fediblock]
    # @param :hostnames [Array<String>]
    def perform(fediblock:, hostnames:)
      instances = ActivityPub::Instance.where(hostname: hostnames)

      # Todos los sitios con la Social Inbox habilitada
      Site.where(id: DeploySocialDistributedPress.pluck(:site_id)).find_each do |site|
        # Crea el estado si no existía
        fediblock_state = site.fediblock_states.find_or_create_by(fediblock: fediblock)

        # No hace nada con los deshabilitados
        next unless fediblock_state.enabled?

        ActivityPub::InstanceModerationJob.perform_later(site: site, hostnames: hostnames)
      end
    end
  end
end
