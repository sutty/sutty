# frozen_string_literal: true

class ActivityPub
  # Obtiene o actualiza los datos de una instancia.  Usamos un cliente
  # de ActivityPub porque la instancia podría estar en federación
  # limitada.
  class InstanceFetchJob < ApplicationJob
    self.priority = 100

    def perform(site:, instance:)
      %w[/api/v2/instance /api/v1/instance].each do |api|
        uri = SocialInbox.generate_uri(instance.hostname) do |u|
          u.path = api
        end

        response = site.social_inbox.dereferencer.get(uri: uri)

        next unless response.success?
        # @todo Validate schema
        next unless response.parsed_response.is_a?(DistributedPress::V1::Social::ReferencedObject)

        instance.update(content: response.parsed_response.object)

        break
      rescue BRS::BaseError,
             Errno::ECONNREFUSED,
             HTTParty::Error,
             JSON::JSONError,
             Net::OpenTimeout,
             OpenSSL::OpenSSLError,
             SocketError,
             Errno::ENETUNREACH => e
        ExceptionNotifier.notify_exception(e, data: { instance: uri })
        break
      end
    end
  end
end
