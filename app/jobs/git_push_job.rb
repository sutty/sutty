# frozen_string_literal: true

# Permite pushear los cambios cada vez que se
# hacen commits en un sitio
class GitPushJob < ApplicationJob
  # @param :site [Site]
  # @return [nil]
  def perform(site)
    @site = site

    site.repository.push if site.repository.origin
  end
end
