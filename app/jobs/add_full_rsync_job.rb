# frozen_string_literal: true

# Agrega un nodo nuevo en segundo plano y sincroniza todos los sitios
class AddFullRsyncJob < ApplicationJob

  # Obtiene todos los sitios que estén sincronizando con un nodo de
  # Sutty, agrega el nodo nuevo y empieza la sincronización.
  #
  # @param :hostname [String] El nombre del servidor remoto
  # @param :destination [String] La ubicación de rsync
  def perform(hostname:, destination:)
    site_ids = DeployFullRsync.all.distinct.pluck(:site_id)

    Site.where(id: site_ids).find_each do |site|
      site
        .deploys
        .create(
          type: 'DeployFullRsync',
          destination: destination,
          hostname: hostname
        ).deploy
    end
  end
end
