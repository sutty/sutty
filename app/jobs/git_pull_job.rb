# frozen_string_literal: true

# Permite traer los cambios desde el repositorio remoto
class GitPullJob < ApplicationJob
  # @param :site [Site]
  # @param :usuarie [Usuarie]
  # @param :message [String]
  # @return [nil]
  def perform(site, usuarie, message)
    @site = site

    return unless site.repository.origin

    site.repository.fetch

    return if site.repository.up_to_date?

    if site.repository.fast_forward?
      site.repository.fast_forward!
    else
      site.repository.merge(usuarie, message)
    end

    site.repository.git_lfs_checkout
    site.reindex_changes!

    nil
  end
end
