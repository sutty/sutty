# frozen_string_literal: true

# Renueva los tokens de Distributed Press antes que se venzan,
# activando los callbacks que hacen que se refresque el token.
class RenewDistributedPressTokensJob < ApplicationJob
  # Renueva todos los tokens a punto de vencer o informa el error sin
  # detener la tarea si algo pasa.
  def perform
    DistributedPressPublisher.with_about_to_expire_tokens.find_each do |publisher|
      publisher.save
    rescue DistributedPress::V1::Error => e
      data = { instance: publisher.instance, expires_at: publisher.client.token.expires_at }

      ExceptionNotifier.notify_exception(e, data: data)
    end
  end
end
