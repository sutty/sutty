# frozen_string_literal: true

# Envía correos de mantenimiento
class MaintenanceMailer < ApplicationMailer
  # Notifica a les usuaries de tareas de mantenimiento
  def notice
    I18n.with_locale params[:lang] do
      attachments['maintenance.ics'] = ics

      mail to: params[:email],
           subject: I18n.t('maintenance_mailer.notice.subject')
    end
  end

  # Notifica que volvimos
  def were_back
    I18n.with_locale params[:lang] do
      mail to: params[:email],
           subject: I18n.t('maintenance_mailer.were_back.subject')
    end
  end

  private

  def cache_key
    @cache_key ||= params[:maintenance].cache_key_with_version + '/ics/' + params[:lang]
  end

  def ics
    Rails.cache.fetch(cache_key, expires_in: 1.hour) do
      cal = Icalendar::Calendar.new
      cal.event do |e|
        e.dtstart = params[:maintenance].estimated_from
        e.dtend = params[:maintenance].estimated_to
        e.summary = I18n.t('maintenance_mailer.ics.summary')
        e.description = params[:maintenance].message
      end

      { mimetype: 'text/calendar; charset=utf-8', content: cal.to_ical }
    end
  end
end
