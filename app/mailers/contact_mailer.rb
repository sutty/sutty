# frozen_string_literal: true

# Formulario de contacto
class ContactMailer < ApplicationMailer
  attr_reader :form

  # Enviar el formulario de contacto a les usuaries
  def notify_usuaries
    subject = "[#{site.title}] #{params[:form_name].humanize}"
    params[:form_definition] = site.form(params[:form_name])

    attachments[params[:form_name] + '.csv'] = generate_csv

    mail to: params[:usuaries_emails],
         reply_to: params[:form][:from],
         subject: subject
  end

  private

  # El CSV es un archivo adjunto con dos filas, una con las etiquetas de
  # los campos en la cabecera y otra con los valores.
  #
  # TODO: Si el sitio tiene muches usuaries esto se genera cada vez.
  def generate_csv
    csv = ["\xEF\xBB\xBF"]
    csv << params[:form].keys.map do |field|
      params[:form_definition].t(field)
    end.to_csv(col_sep: ';', force_quotes: true)

    csv << params[:form].values.to_csv(col_sep: ';', force_quotes: true)

    {
      mime_type: 'text/csv; charset=utf-8',
      content: csv.join
    }
  end
end
