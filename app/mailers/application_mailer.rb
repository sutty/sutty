# frozen_string_literal: true

# Configuración base del correo
class ApplicationMailer < ActionMailer::Base
  helper :application
  before_action :inline_logo!

  layout 'mailer'

  private

  def site
    @site ||= @params[:site]
  end

  def inline_logo!
    attachments.inline['logo.png'] ||=
      File.read(Rails.root.join('app', 'assets', 'images', 'logo.png'))
  end
end
