# Directorios

Sutty se maneja con una estructura de directorios donde cada habitante
tiene un directorio de trabajo con los sitios a los que tiene acceso.

En un deploy con capistrano, se vería así:

    ls /srv/http/sutty.kefir.red/shared/_sites

    cyber-women.com

    ls /srv/http/sutty.kefir.red/shared/_usuarias

    hola@kefir.red
    persona@kefir.red

Donde `hola@kefir.red` es el directorio de trabajo de la habitante.
Dentro de ese, están los sitios:

    ls /srv/http/sutty.kefir.red/shared/_usuarias/hola@kefir.red

    cyber-women.com

Cada sitio se almacena (o se vincula) en `_sites` y está identificado
por su nombre de dominio, para que sea más simple saber de qué se trata.

Si quisiéramos dar acceso a `persona@kefir.red` solo hay que hacer un
link simbólico.

    cd /srv/http/sutty.kefir.red/shared/_usuarias/persona@kefir.red
    ln -sv ../../_sites/cyber-women.com .

De esta forma vinculamos el sitio `cyber-women.com` a la cuenta
`persona@kefir.red`.

## Deploy

Los sitios compilados por Sutty se almacenan en un directorio `_deploy`
con su nombre de dominio:

    ls /srv/http/sutty.kefir.red/shared/_deploy

    cyber-women.com

Para publicar este sitio, hay que crear un link simbólico al directorio
desde el que el servidor web puede leer (todavía no tenemos una
integración entre nginx y sutty que lea sitios directamente desde
`_deploy`).

    cd /srv/http
    ln -sv /srv/http/sutty.kefir.red/shared/_deploy/cyber-women.com .

Y en la configuración de nginx:

    cat /etc/nginx/sites/cyber-women.com.conf

    server {
      server_name cyber-women.com;
    }

