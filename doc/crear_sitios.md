# Crear sitios

Para que les usuaries puedan crear sitios, vamos a tener el siguiente
flujo:

* Nombre del sitio, en minúsculas, sin puntos.

* Descripción, una descripción de qué hace el sitio para Sutty y otres
  usuaries del sitio.

* Plantilla.  Lista de plantillas disponibles, con captura, nombre, link
  a vista previa, link a autore, licencia y usos posibles (blogs, medios
  alternativos, denuncias, etc.)

  Tengo mi propio diseño! Explicar que vamos a dar esta posibilidad más
  adelante, pero que necesitamos financiamiento.  Esto permitiría
  agregar un repositorio o gema de plantilla.

* Licencia.  Elegir la licencia del contenido de un listado con
  licencias piolas:

  * PPL
  * CC-BY
  * CC-BY-SA
  * CC-0

* Lugares donde se van a subir los sitios.  Por defecto nombre.sutty.nl,
  otras opciones en un desplegable:

  * Tengo mi propio dominio: Explica que todavía no tenemos esto
    autogestionado pero que si quieren apoyar el trabajo que hacemos
    pueden donarnos o ponerse en contacto.

    Más adelante pide los dominios, explica cómo comprarlos en njal.la y
    qué información poner los DNS para poder alojarlos.  Cuando Sutty
    tenga su propio DNS, indica los NS.

  * Neocities: Explicar qué es neocities y permitir agregar una cuenta.
    Podríamos varias pero queremos estar bien con neocities y además no
    tiene mucho sentido tener varias páginas en el mismo host.

    Pedir usuarie y contraseña o API key.  Dar link directo a dónde
    sacar la API key y explicar para qué es.

    **En realidad esta sería nuestra primera opción?**

  * Zip: Descargar el sitio en un archivo zip, proponiendo posibles usos
    offline (raspberries, etc!)

    Da una URL desde donde se puede descargar el sitio.

  * SSH/SFTP: Explicar que permite enviar el sitio a servidores propios.
    Permite agregar varios servidores, pide usuario, dominio y puerto.
    Da la llave pública SSH de Sutty y explica cómo agregarla al
    servidor remoto.  También da un link de descarga para que puedan
    hacer ssh-copy-id.

    **Esto todavía no**

  * IPFS: Da la opción de activar/desactivar soporte para IPFS.
    Explica qué es y para qué sirve.  Vincula a documentación sobre
    instalar IPFS de escritorio y cómo pinear el hash de sutty, instalar
    el companion, etc.

    **Esto todavía no**

  * Torrent: Genera un torrent y lo siembra.

    **Esto todavía no**

  * Syncthing: Explica qué es y da la ID del nodo introductor de Sutty.

    **Esto todavía no**

  * Zeronet: Idem syncthing?

    **Esto todavía no**

  * Archive.org

    **Esto todavía no**

* Crear sitio!

## Sitios

Tenemos un sitio esqueleto que tiene lo básico para salir andando:

* Gemas de Jekyll
* Gemas de Sutty
* Gemas de Temas
* Esqueleto de la configuración
* Directorios base (con i18n también)

El sitio esqueleto es un repositorio Git que se clona al directorio del
sitio.  Esto permite luego pullear actualizaciones desde el esqueleto a
los sitios, esperamos que sin conflictos!

## Diseño

Los diseños son plantillas Jekyll adaptadas a Sutty.  Vamos a empezar
adaptando las que estén disponibles en <https://jekyllthemes.org/> y
otras fuentes, agregando features de Sutty y simplificando donde haga
falta (algunas plantillas tienen requisitos extraños).

Las plantillas se instalan como gemas en los sitios, de forma que
podemos cambiarla desde las opciones del sitio luego.

Para poder cambiar el diseño, necesitamos poder editar la configuración
del sitio en _config.yml y colocar el nombre de la gema en `theme:`

## Internamente

Al crear un sitio, clonar el esqueleto en el lugar correcto.  Al
eliminarlo, eliminar el directorio.

Lo correcto sería preguntar a todes les usuaries si están de acuerdo en
borrar el sitio.  Si una no está de acuerdo, el borrado se cancela.

### Licencias

Las licencias disponibles se pueden gestionar desde la base de datos.
Los atributos son:

* Titulo
* URL
* Descripción, por qué la recomendamos, etc.

El problema que tenemos es que las queremos tener traducidas, entonces
hay varias opciones:

* Incorporar columna idioma en la base de datos y cada vez que se
  muestren las licencias filtrar por idioma actual (o idioma por
  defecto).

  Esto nos permitiría ofrecer licencias por jurisdicción también, aunque
  empezaríamos con las internacionales...

* Incorporar la gema de traducción y poner las traducciones en la base
  de datos.  Esto permite tener una sola licencia con sus distintas
  traducciones en un solo registro.

  Pensábamos que necesitábamos cambiar a PostgreSQL, pero la gema
  [Mobility](https://github.com/shioyama/mobility) permite usar
  distintas estrategias.

* Incorporar las traducciones a los locales de sutty.  Esta es la opción
  menos flexible, porque implica agregar licencias a la base de datos y
  al mismo tiempo actualizar los archivos y re-deployear Sutty... mejor
  no.

Pero es importante que estén asociadas entre sí por idioma.

Permitir que les usuaries elijan licencia+privacidad+codigo de
convivencia e informarles que van a ser los primeros artículos dentro de
su sitio y que los pueden modificar después.  Que esta es nuestra
propuesta tecnopolítica para que los espacios digitales y analógicos
sean espacios amables siguiente una lógica de cuidados colectivos.

## Actualizar skel

Cuando actualizamos el skel, sutty pide a todos los sitios
consentimiento para aplicar las actualizaciones.  Antes de aplicar las
actualizaciones muestra el historial para que les usuaries vean cuales
son los cambios que se van a aplicar.  Este historial viene del
repositorio git con lo que tenemos que tomarnos la costumbre de escribir
commits completos con explicación.

## Alojamiento

Para elegir las opciones de alojamiento, agregamos clases que
implementan el nombre del alojamiento y sus opciones específicas:

* Local (sitio.sutty.nl): Deploy local.  Genera el sitio en
  _deploy/name.sutty.nl y lo pone a disposición del servidor web (es
  como veniamos trabajando hasta ahora).  También admite dominios
  propios.

  Solo se puede tener uno y no es opcional porque lo necesitamos para
  poder hacer el resto de deploys

* Neocities: Deploy a neocities.org, necesita API key.  Se conecta con
  neocities usando la API y envía los archivos.

  Solo se puede tener uno (pendiente hablar con neocities si podemos
  hacer esto).

* Zip: genera un zip con el contenido del sitio y provee una url de
  descarga estilo https://sutty.nl/sites/site.zip

  Solo se puede tener uno

* SSH: Deploy al servidor SSH que especifiquemos.  Necesita que le
  pasemos user@host, puerto y ruta (quizás la ruta no es necesaria y
  vaya junto con user@host).  Informar a les usuaries la llave pública
  de Sutty para que la agreguen.

  No hay límite a los servidores SSH que se pueden agregar.

La clase Deploy::* tiene que implementar estas interfaces:

  * `#limit` devuelve un entero que es la cantidad de deploys de este
    tipo que se pueden agregar, `nil` significa sin límite
  * `#deploy` el método que realiza el deploy.  En realidad este método
    hay que correrlo por fuera de Sutty...
  * `#attributes` los valores serializados que acepta el Deploy

Son modelos que pueden guardar atributos en la base y a través de los
que se pueden tomar los datos a través de la API (cuando tengamos un
método de deployment).

El plan es migrar todo esto a <drone.io> de forma que la compilación se
haga por separado de sutty.  Este es un plan intermedio hasta que
tengamos tiempo de hacerlo realmente.


# TODO

* aplicar la licencia al sitio!
* ver las estadisticas de compilación en lugar del log (el log también)

  agrupar los build stats para poder ver todos los pasos de una
  compilación juntos
* comitear en git los articulos (igual no es de esta rama...)
* link a visitar sitio
* editor de opciones
* forkear gemas
