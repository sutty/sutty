# Desarrollo

## Instalar el entorno de trabajo

* Instalar las herramientas de compilación de la distribución GNU/Linux
  que estemos usando:

```bash
sudo apt install build-essential # debian, ubuntu y derivadas
sudo pacman -S base-devel # arch y derivadas
```

* Clonar el repositorio e ingresar al directorio (siempre hay que
  ingresar al directorio para desarrollar Sutty)

```bash
git clone https://0xacab.org/sutty/sutty.git
```

* [Instalar rbenv](https://github.com/rbenv/rbenv)
* [Instalar rbenv-build](https://github.com/rbenv/ruby-build)
* Instalar la versión de Ruby necesaria para Sutty:

```bash
cat .rbenv-version | xargs rbenv install
```

* Instalar bundler y bundler-audit e instalar las gemas (hay que correr
  bundle cada vez que cambia Gemfile o Gemfile.lock)

```bash
gem install bundler bundler-audit
bundle
```

* Instalar `nodejs` y `yarn` e instalar las dependencias (las
  dependencias se instalan cada vez que se modifica el archivo
  `package.json`)

```bash
sudo apt install nodejs yarn # debian...
sudo pacman -S nodejs yarn # arch...
yarn
```

* Crear la base de datos

```bash
bundle exec rake db:setup
```

## Correr el servidor de prueba

* Iniciar el servidor de rails

```bash
bundle exec rails s # s de server
```

* Visitar <http://localhost:3000/> con un navegador

## Correr los tests

```bash
bundle exec rake test
```
