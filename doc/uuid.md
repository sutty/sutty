# Identificadores para los artículos

Para poder vincular artículos entre sí y para otros usos, necesitamos
identificarlos únicamente.  Un identificador incremental es problemático
porque tendríamos que mantener el estado y poder responder preguntas
como ¿cuál es el último identificador que asignamos?

Para poder identificar artículos sin mantener estado, usamos
[UUIDs](https://en.wikipedia.org/wiki/Universally_unique_identifier),
que son cadenas aleatorias que se pueden asignar adhoc.  Así, en lugar
de un ID numérico que va incrementando, podemos asociar cadenas al
estilo `fb4a5048-5fa1-4b85-b70e-6c502feecdb9` (generada con la
herramienta `uuidgen`).

## MetadataUUID

Cada artículo se crea con un metadato `uuid` cuyo valor por defecto es
un UUID autogenerado utilizando `SecureRandom.uuid`.  Este valor no
cambia (a menos que se lo vacíe intencionalmente).

## Migración

Para todos los artículos que existen, hay que escribir una migración que
se los agregue.

Para esto hay que cargar sitio por sitio, recorrer los artículos
asignando un UUID y guardando todos los cambios como un solo commit de
git.
