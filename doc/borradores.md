# Borradores

En Jekyll, los borradores se encuentran en el directorio `_drafts`.

En Sutty, para traducir a distintos idiomas, utilizamos "colecciones"
con el nombre del idioma.  Nuestra extensión de traducciones reemplaza
el directorio `_posts` por cada uno de estos y genera un sitio por cada
idioma.  De esta forma no tenemos que hacer adaptaciones más profundas a
las plantillas.

Hasta ahora lo que hacíamos es agregar un metadata `draft: true` a los
artículos y luego los filtrábamos con una extensión.  Creemos que esta
opción es mínima y suficiente para lo que queremos lograr (excluir
algunos artículos de la compilación).

De lo contrario, tendríamos que implementar lo siguiente, que no es
trivial:

* Mover artículos de lugar entre `_lang/` y `_lang/_drafts`, utilizando
  `mv` y `rugged`.

* Implementar acceso a borradores por colección.  Jekyll carga los
  borradores solo desde `_drafts`, con lo que no podríamos saber a qué
  idioma pertenece a menos que ignoremos ese dato o lo guardemos como
  metadato...

En este momento es más simple para Sutty mantener el `draft: true`.
