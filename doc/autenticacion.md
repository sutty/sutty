# Autenticación de usuaries

Estamos pasando de un modelo integrado donde Usuarias son usuaries de
una red IMAP e Invitadxs son usuaries locales de la plataforma, a una
más "monolítica" donde las cuentas se gestionan desde la plataforma
misma.

Entonces, Usuaria e Invitadx se fusionan y su diferencia es solo de
privilegios sobre un sitio (puede hacer todo / solo puede cargar
artículos y modificar los propios).

No nos gusta la idea de implementar todo un sistema de privilegios,
primero porque queremos que Sutty sea una plataforma democrática y
segundo porque en nuestra experiencia nadie los usa y prefieren usar una
cuenta de administración para todo.

La migración a Devise nos va a permitir tener recuperación de
contraseñas, registro independiente, correos de bienvenida y varias
cosas más.

Planeamos que Sutty también sea un proveedor de oAuth, para poder
integrarla con otras plataformas comunitarias
(rocket.chat/mattermost.com principalmente).

## Base de datos

Los Sitios tienen una contraparte física, de archivos, pero también se
crean en la base de datos para establecer relaciones con Usuaries.

Une Usuarie puede tener muchos Sitios y viceversa.

También queremos saber qué rol tienen, con lo que tenemos una tabla de
roles que establece el rol que une usuarie tiene en un sitio.  De lo
contrario necesitamos establecer roles y ya entramos en las dificultades
que decíamos más arriba.

Podemos saber quién es invitade ingresando a un sitio y fijándonos si
está en su lista de invitade.  Lo mismo para usuaries.

Les usuaries pueden bloquear invitades y a otres usuaries y sumar
usuaries e invitades a su sitio (via correo de invitación).

## Invitaciones a sitios

### Enviar invitación

Desde la gestión del sitio se puede invitar a nueves usuaries e
invitades.  Se les envía un correo (y cuando tengamos sistema de
notificaciones, una notificación) donde pueden confirmar su
participación.

Si no tienen cuenta, tienen que registrarse completando los datos en el
momento, sino se pueden loguear normalmente.

Para poder hacer una invitación con consentimiento, se guarda el rol
como temporal.  Cuando la usuaria acepta la invitación el rol se vuelve
definitivo.

### Aceptar invitación

Al ingresar, le usuarie ve su listado de sitios y en la lista ve a
cuales está invitade.  Usa el botón de aceptar invitación para poder
acceder al sitio.  Si rechaza la invitación, el rol se elimina de la
base de datos.

Eventualmente queremos pasar a un modelo de estados del rol donde
podamos saber si fue rechazado, aceptado, etc.

### Invitades desde la web

Al publicar la URL de invitación, les invitades se puedan registrar
por su cuenta.  Solo deben completar sus datos (correo y contraseña) y
reciben un correo de confirmación.

En la configuración del sitio hay que distinguir entre invitades por
invitación o por registro automático.

# Falta

Darle estilo a los mails
