# Reordenar los articulos

Todos los posts tienen un campo `order`.

El orden se actualiza en base al orden cronológico.

En la plantilla se puede ordenar cronólogicamente o por orden numérico.

El orden es independiente de otros metadatos (como layout, categoria,
etc), todos los artículos siguen un orden

Como el orden es un metadato, tenemos que ignorar los tipos de posts que
no lo tienen

El orden por defecto es orden natural, más bajo es más alto.

El problema que tiene esta implementación es que al reordenar los posts
necesitamos mantener el orden original sobre el que estabamos ordenando,
sino terminamos aplicando un orden incorrecto.  Esto requiere que
implementemos una forma de mantener el orden entre sesiones e incluso
general.  Nos vendría bien para no tener que recargar el sitio una y
otra vez.

Lo más controlado sería enviar exactamente el id del post con su nueva
ubicación en el orden.  Esta es la implementación anterior.

***

El orden es descendiente (fechas más nuevas primero), pero el orden que
estuvimos usando es ascendientes (números más bajos primero).  Es más
simple invertir la lógica y hacer todo el orden descendiente.  Para eso
los artículos más nuevos tienen que tener el número de orden
correspondiente a la posición en el array ordenado por fecha.
