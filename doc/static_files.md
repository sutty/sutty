# Archivos estáticos

El objetivo es encontrar una forma de importar los archivos estáticos de
un sitio que se ha migrado a la estructura de Sutty, es decir, subirlos
como archivos de Rails y luego vincularlos a los artículos.

Actualmente, los archivos primero se suben a la interfaz via
ActiveStorage y luego se vinculan internamente al repositorio[^git].

Lo que deberíamos lograr es reconocer los archivos vinculados desde los
artículos y hacer el proceso de cargarlos internamente, modificando los
artículos para que apunten a las nuevas versiones.

Esto podría hacerse por cada artículo individual o durante el proceso de
creación del sitio, después de clonarlo.

El algoritmo sería:

* Clonar el sitio
* Obtener todos los artículos de todas las colecciones
* Encontrar todos los layouts con archivos adjuntos
* Buscar todos los artículos que tengan esos layouts
* Tomar el archivo físico desde los metadatos y asociarlo al sitio via Active Storage
* Asociar el archivo al MetadataFile/Image correspondiente
* ???
* Migración cumplida!


[^git]: esto puede hacer que los repositorios git sean gigantes,
  podríamos usar algo como git-annex quizás?
