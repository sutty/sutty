# frozen_string_literal: true

namespace :stats do
  desc 'Process stats'
  task process_all: :environment do
    Site.all.find_each do |site|
      UriCollectionJob.perform_now site: site, once: true
      StatCollectionJob.perform_now site: site, once: true
    end
  end
end
