# frozen_string_literal: true

namespace :activity_pub do
  desc 'Update Fediblocks'
  task fediblocks: :environment do |_, args|
    ActivityPub::FediblockFetchJob.perform_later
  end
end
