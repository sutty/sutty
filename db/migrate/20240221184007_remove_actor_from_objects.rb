# frozen_string_literal: true

# No es necesario vincular actores con objetos, porque la forma en que
# lo estábamos haciendo no se refiere a le actore del objeto, sino de
# acciones distintas sobre el mismo objeto, generado por une actore.
#
# Y ese valor ya lo podemos obtener desde attributedTo
class RemoveActorFromObjects < ActiveRecord::Migration[6.1]
  def change
    remove_column :activity_pub_objects, :actor_id, :uuid, index: true
  end
end
