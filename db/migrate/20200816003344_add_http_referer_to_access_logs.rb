# frozen_string_literal: true

# Con access_log v0.3 podemos decidir la política de retención de
# referencias.
class AddHttpRefererToAccessLogs < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    add_column :access_logs, :http_referer, :string, index: true
  end
end
