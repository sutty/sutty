# frozen_string_literal: true

# Crea la tabla de publishers de Distributed Press que contiene las
# instancias y tokens
class CreateDistributedPressPublisher < ActiveRecord::Migration[6.1]
  def change
    create_table :distributed_press_publishers do |t|
      t.timestamps
      t.string :instance, unique: true
      t.text :token_ciphertext, null: false
      t.datetime :expires_at, null: true
    end
  end
end
