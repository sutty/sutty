# frozen_string_literal: true

# Habilitar las extensiones de búsqueda de texto libre
class CreatePgSearchExtensions < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'plpgsql'
    enable_extension 'pg_trgm'
  end
end
