# frozen_string_literal: true

# Almacena las instancias
class CreateActivityPubInstances < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pub_instances, id: :uuid do |t|
      t.timestamps
      t.string :hostname, index: true, unique: true, null: false
      t.string :aasm_state, null: false
      t.jsonb :content, default: {}
    end
  end
end
