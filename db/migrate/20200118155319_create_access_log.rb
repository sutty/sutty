# frozen_string_literal: true

class CreateAccessLog < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    create_table :access_logs, id: :uuid do |t|
      t.string :host, index: true

      t.float :msec

      t.string :server_protocol
      t.string :request_method
      t.string :request_completion

      t.string :uri, index: true
      t.string :query_string
      t.integer :status, index: true

      t.string :sent_http_content_type
      t.string :sent_http_content_encoding
      t.string :sent_http_etag
      t.datetime :sent_http_last_modified

      t.string :http_accept
      t.string :http_accept_encoding
      t.string :http_accept_language

      t.string :http_pragma
      t.string :http_cache_control
      t.string :http_if_none_match

      t.string :http_dnt
      t.string :http_user_agent, index: true
      t.string :http_referer, index: true

      t.float :request_time

      t.integer :bytes_sent
      t.integer :body_bytes_sent
      t.integer :request_length

      t.string :http_connection
      t.string :pipe
      t.integer :connection_requests

      t.string :geoip2_data_country_name, index: true
      t.string :geoip2_data_city_name, index: true

      t.string :ssl_server_name
      t.string :ssl_protocol
      t.string :ssl_early_data
      t.string :ssl_session_reused
      t.string :ssl_curves
      t.string :ssl_ciphers
      t.string :ssl_cipher

      t.string :sent_http_x_xss_protection
      t.string :sent_http_x_frame_options
      t.string :sent_http_x_content_type_options
      t.string :sent_http_strict_transport_security

      t.string :nginx_version
      t.integer :pid
    end
  end
end
