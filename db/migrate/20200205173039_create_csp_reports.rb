class CreateCspReports < ActiveRecord::Migration[6.0]
  def change
    create_table :csp_reports, id: :uuid do |t|
      t.timestamps

      t.string :disposition
      t.string :referrer
      t.string :blocked_uri
      t.string :document_uri
      t.string :effective_directive
      t.string :original_policy
      t.string :script_sample
      t.string :status_code
      t.string :violated_directive
    end
  end
end
