class AddInvitadesToSites < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :acepta_invitades, :boolean, default: false
  end
end
