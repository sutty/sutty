# frozen_string_literal: true

# Agrega el orden y fecha del post en el post indexado, de forma que los
# resultados se puedan obtener en el mismo orden.
class AddOrderToIndexedPosts < ActiveRecord::Migration[6.1]
  def change
    add_column :indexed_posts, :order, :integer, default: 0
  end
end
