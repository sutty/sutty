# Crea Usuaries y migra lxs Invitadxs pre-existentes.
class CreateUsuarie < ActiveRecord::Migration[5.2]
  def up
    create_table :usuaries do |t|
      t.timestamps
      t.string :email,              null: false, default: ''
      t.string :encrypted_password, null: false, default: ''

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # Only if lock strategy is :failed_attempts
      t.integer  :failed_attempts, default: 0, null: false
      # Only if unlock strategy is :email or :both
      t.string   :unlock_token
      t.datetime :locked_at

      t.boolean  :acepta_politicas_de_privacidad, default: false
    end

    add_index :usuaries, :email,                unique: true
    add_index :usuaries, :reset_password_token, unique: true
    add_index :usuaries, :confirmation_token,   unique: true
    add_index :usuaries, :unlock_token,         unique: true

    Invitadx.all.find_each do |i|
      Usuarie.create email: i.email,
                     encrypted_password: i.password_digest,
                     acepta_politicas_de_privacidad: i.acepta_politicas_de_privacidad,
                     confirmed_at: i.confirmed ? i.updated_at : nil,
                     updated_at: i.updated_at,
                     created_at: i.created_at
    end

    drop_table :invitadxs
  end

  def down
    create_table :invitadxs do |t|
      t.timestamps
      t.string :password_digest
      t.string :email, unique: true, index: true
      t.string :confirmation_token
      t.boolean :confirmed
      t.boolean :acepta_politicas_de_privacidad, default: false
    end

    Usuarie.all.find_each do |i|
      Invitadx.create email: i.email,
                      password_digest: i.encrypted_password,
                      acepta_politicas_de_privacidad: i.acepta_politicas_de_privacidad,
                      confirmed: !i.confirmed_at.nil?,
                      updated_at: i.updated_at,
                      created_at: i.created_at
    end

    drop_table :usuaries
  end
end
