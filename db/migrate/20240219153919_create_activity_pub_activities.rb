# frozen_string_literal: true

# Actividades.  Se asocian a un objeto y a una cola de moderación
class CreateActivityPubActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pub_activities, id: :uuid do |t|
      t.timestamps

      t.uuid :activity_pub_id, index: true, null: false

      t.string :type, null: false
      t.string :uri, null: false
      t.jsonb :content, default: {}
    end
  end
end
