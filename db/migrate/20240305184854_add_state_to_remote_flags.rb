# frozen_string_literal: true

# Estado de los reportes remotos
class AddStateToRemoteFlags < ActiveRecord::Migration[6.1]
  def change
    add_column :activity_pub_remote_flags, :aasm_state, :string, null: false, default: 'waiting'
  end
end
