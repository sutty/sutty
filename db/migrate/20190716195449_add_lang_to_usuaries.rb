# frozen_string_literal: true

# Agrega la columna de idioma a cada usuarie para que pueda ver el sitio
# y escribir artículos en su idioma.
class AddLangToUsuaries < ActiveRecord::Migration[5.2]
  def change
    add_column :usuaries, :lang, :string, default: 'es'
  end
end
