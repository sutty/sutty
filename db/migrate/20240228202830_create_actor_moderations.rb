# frozen_string_literal: true

# Relación entre Actor y Site
class CreateActorModerations < ActiveRecord::Migration[6.1]
  def change
    create_table :actor_moderations, id: :uuid do |t|
      t.timestamps

      t.belongs_to :site
      t.uuid :actor_id, index: true
      t.string :aasm_state, null: false
    end
  end
end
