# frozen_string_literal: true

# Renombrar todos los repositorios que apunten a skel como su origin
class SiteRenameOriginToUpstream < ActiveRecord::Migration[6.1]
  # Renombrar
  def up
    Site.find_each do |site|
      next unless site.repository.origin&.url == ENV['SKEL_SUTTY']

      site.repository.rugged.remotes.rename('origin', 'upstream') do |_|
        Rails.logger.info "#{site.name}: renamed origin to upstream"
      end
    rescue Rugged::Error, Rugged::OSError => e
      Rails.logger.warn "#{site.name}: #{e.message}"
    end
  end

  # No se puede deshacer
  def down; end
end
