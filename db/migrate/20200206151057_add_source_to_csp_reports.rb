class AddSourceToCspReports < ActiveRecord::Migration[6.0]
  def change
    add_column :csp_reports, :column_number, :integer
    add_column :csp_reports, :line_number, :integer
    add_column :csp_reports, :source_file, :string
  end
end
