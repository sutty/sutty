# frozen_string_literal: true

# Almacena objetos de ActivityPub.  Los objetos pueden estar compartidos
# por toda la instancia.
class CreateActivityPubObjects < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pub_objects, id: :uuid do |t|
      t.timestamps

      t.uuid :actor_id, index: true, null: false

      t.string :type, null: false
      t.string :uri, null: false, unique: true
      t.jsonb :content, default: {}
    end
  end
end
