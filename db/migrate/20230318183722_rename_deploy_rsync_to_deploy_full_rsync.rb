# frozen_string_literal: true

# Cambia todos los DeployRsync propios de Sutty a DeployFullRsync que se
# encarga de sincronizar todo.
class RenameDeployRsyncToDeployFullRsync < ActiveRecord::Migration[6.1]
  def up
    DeployRsync.all.find_each do |deploy|
      dest = deploy.destination.split(':', 2).first

      next unless nodes.include? dest

      deploy.destination = "#{dest}:"
      deploy.type = 'DeployFullRsync'

      deploy.save
    end
  end

  def down
    DeployFullRsync.all.find_each do |deploy|
      next unless nodes.include? deploy.destination.split(':', 2).first

      deploy.destination = "#{deploy.destination}#{deploy.site.hostname}"
      deploy.type = 'DeployRsync'

      deploy.save
    end
  end

  private

  def nodes
    @nodes ||= Rails.application.nodes.map do |node|
      "sutty@#{node}"
    end
  end
end
