# frozen_string_literal: true

# Crea la tabla de sitios y las relaciones con Usuaries e Invitades
class CreateSitios < ActiveRecord::Migration[5.2]
  def up
    # Tabla de Sitios
    create_table :sites do |t|
      t.timestamps
      t.string :name, index: true, unique: true
    end

    # Relación HABTM entre Sitios y Usuaries
    create_table :sites_usuaries, id: false do |t|
      t.belongs_to :site, index: true
      t.belongs_to :usuarie, index: true
    end

    # Relación HABTM entre Sitios e Invitades
    create_table :invitades_sites, id: false do |t|
      t.belongs_to :site, index: true
      t.belongs_to :usuarie, index: true
    end

    ## Crear las relaciones correspondientes
    #
    # Obtener todos los sitios (estamos moviendo funcionalidad previa de
    # Site
    sites = Pathname.new(Site.site_path)
                    .children.map(&:expand_path).map(&:to_s)

    sites.each do |dir|
      site = Site.create name: File.basename(dir)

      file = File.join([dir, '.usuarias'])
      usuarias = File.exist?(file) ? File.read(file).split("\n").uniq : []

      file = File.join([dir, '.invitadxs'])
      invitadxs = File.exist?(file) ? File.read(file).split("\n").uniq : []

      # Tenemos que crear a les usuaries porque no existían en ningún
      # lado
      usuarias.each do |email|
        usuarie = Usuarie.find_by(email: email)
        usuarie ||= Usuarie.create(email: email,
                                   password: SecureRandom.hex,
                                   confirmed_at: Date.today)

        sql = "insert into sites_usuaries (site_id, usuarie_id)
          values (#{site.id}, #{usuarie.id});"
        ActiveRecord::Base.connection.execute(sql)
      end

      invitadxs.each do |email|
        usuarie = Usuarie.find_by(email: email)
        usuarie ||= Usuarie.create(email: email,
                                   password: SecureRandom.hex,
                                   confirmed_at: Date.today)
        sql = "insert into invitades_sites (site_id, usuarie_id)
          values (#{site.id}, #{usuarie.id});"
        ActiveRecord::Base.connection.execute(sql)
      end
    end
  end

  def down
    drop_table :sites
    drop_table :sites_usuaries
    drop_table :invitades_sites
  end
end
