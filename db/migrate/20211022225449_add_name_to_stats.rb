# frozen_string_literal: true

# Agregarle un nombre a la estadística
class AddNameToStats < ActiveRecord::Migration[6.1]
  def change
    add_column :stats, :name, :string, null: false
    add_index :stats, :name, using: 'hash'
  end
end
