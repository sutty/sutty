# frozen_string_literal: true

class AddAceptaPoliticasDePrivacidadToInvitadx < ActiveRecord::Migration[5.1]
  def change
    add_column :invitadxs, :acepta_politicas_de_privacidad, :boolean, default: false
  end
end
