# frozen_string_literal: true

# Agrega políticas de privacidad
class AddPrivacyPolicy < ActiveRecord::Migration[6.1]
  def up
    create_table :privacy_policies do |t|
      t.timestamps
      t.string :title
      t.text :description
      t.text :content
    end

    # XXX: En lugar de ponerlo en las seeds
    YAML.safe_load(File.read('db/seeds/privacy_policies.yml')).each do |pp|
      PrivacyPolicy.new(**pp).save!
    end
  end

  def down
    drop_table :privacy_policies
  end
end
