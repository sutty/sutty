# frozen_string_literal: true

# Crea la tabla de roles
class CreateRoles < ActiveRecord::Migration[5.2]
  def up
    create_table :roles do |t|
      t.timestamps
      t.belongs_to :site
      t.belongs_to :usuarie
      t.string     :rol
      t.boolean    :temporal

      # Solo podemos tener un rol a la vez
      t.index %i[site_id usuarie_id], unique: true
    end
  end

  def down; end
end
