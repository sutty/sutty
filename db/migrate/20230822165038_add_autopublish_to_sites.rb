# frozen_string_literal: true

# Agrega posibilidad de autopublicación del sitio
class AddAutopublishToSites < ActiveRecord::Migration[6.1]
  def change
    add_column :sites, :auto_publish, :boolean, default: false
  end
end
