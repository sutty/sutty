# frozen_string_literal: true

# Agrega la columna de nodo a los logs
class AddNodeToAccessLogs < ActiveRecord::Migration[6.1]
  def change
    add_column :access_logs, :node, :string, index: true
  end
end
