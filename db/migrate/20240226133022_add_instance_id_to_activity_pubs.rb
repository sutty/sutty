# frozen_string_literal: true

# Relaciona instancias con sus actividades
class AddInstanceIdToActivityPubs < ActiveRecord::Migration[6.1]
  def up
    add_column :activity_pubs, :instance_id, :uuid, index: true

    ActivityPub.all.find_each do |activity_pub|
      activity_pub.update(instance: activity_pub&.object&.actor&.instance)
    end
  end

  def down
    remove_column :activity_pubs, :instance_id, :uuid, index: true
  end
end
