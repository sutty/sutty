# frozen_string_literal: true

# Ya no es necesario reindexar por la fuerza
class DeprecateDeployReindex < ActiveRecord::Migration[6.1]
  def up
    Deploy.where(type: 'DeployReindex').destroy_all
  end

  def down;end
end
