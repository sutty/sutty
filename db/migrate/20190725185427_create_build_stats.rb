# frozen_string_literal: true

# Crea la tabla de estadísticas de compilación
class CreateBuildStats < ActiveRecord::Migration[5.2]
  def change
    create_table :build_stats do |t|
      t.timestamps
      t.belongs_to :deploy, index: true
      t.integer :bytes
      t.float :seconds
      t.string :action, null: false
      t.text :log
    end
  end
end
