# frozen_string_literal: true

# Comprueba que se pueden volver a correr las tareas que dieron error de
# decompresión
class BrsDecompressorCorruptedSourceError < ActiveRecord::Migration[6.1]
  def up
    raise unless HTTParty.get('https://mas.to/api/v2/instance',
                              headers: { 'Accept-Encoding': 'br;q=1.0,gzip;q=1.0,deflate;q=0.6,identity;q=0.3' }).ok?

    QueJob.where("last_error_message like '%BRS::DecompressorCorruptedSourceError%'").update_all(error_count: 0,
                                                                                                 run_at: Time.now)
  end

  def down; end
end
