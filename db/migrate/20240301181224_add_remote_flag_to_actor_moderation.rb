# frozen_string_literal: true

# Las acciones de moderación pueden tener un reporte remoto asociado
class AddRemoteFlagToActorModeration < ActiveRecord::Migration[6.1]
  def up
    add_column :actor_moderations, :remote_flag_id, :uuid, null: true

    ActivityPub::RemoteFlag.all.find_each do |remote_flag|
      actor_moderation = ActorModeration.find_by(actor_id: remote_flag.actor_id)

      actor_moderation&.update_column(:remote_flag_id, remote_flag.id)
    end
  end

  def down
    remove_column :actor_moderations, :remote_flag_id
  end
end
