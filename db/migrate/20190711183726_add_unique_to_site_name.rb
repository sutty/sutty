# frozen_string_literal: true

# Los nombres de los sitios son únicos
class AddUniqueToSiteName < ActiveRecord::Migration[5.2]
  def change
    remove_index :sites, :name
    add_index :sites, :name, unique: true
  end
end
