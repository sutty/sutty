# frozen_string_literal: true

# Agrega la opción de paginación a los sitios
class AddPaginationToSite < ActiveRecord::Migration[6.1]
  def change
    add_column :sites, :pagination, :boolean, default: false
  end
end
