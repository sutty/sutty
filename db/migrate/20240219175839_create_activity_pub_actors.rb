# frozen_string_literal: true

# Almacena actores de ActivityPub y los relaciona con actividades
class CreateActivityPubActors < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pub_actors, id: :uuid do |t|
      t.timestamps
      t.uuid :instance_id, index: true, null: false
      t.string :uri, index: true, unique: true, null: false
    end
  end
end
