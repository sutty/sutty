# frozen_string_literal: true

# Las fediblocks son listas descargables de instancias bloqueadas.  El
# formato hace una recomendación sobre suspensión o desfederación, pero
# nosotres bloqueamos todo.
class CreateFediblocks < ActiveRecord::Migration[6.1]
  def up
    create_table :activity_pub_fediblocks, id: :uuid do |t|
      t.timestamps

      t.string :title, null: false
      t.string :url, null: false
      t.string :download_url, null: false
      t.string :format, null: false
      t.jsonb  :hostnames, default: []
    end

    YAML.safe_load(File.read('db/seeds/activity_pub/fediblocks.yml')).each do |fediblock|
      ActivityPub::Fediblock.create(**fediblock).process!
    end
  end

  def down
    drop_table :activity_pub_fediblocks
  end
end
