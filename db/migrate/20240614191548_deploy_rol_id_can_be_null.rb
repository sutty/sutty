# frozen_string_literal: true

# El rol_id no es necesario para todos los deploys
class DeployRolIdCanBeNull < ActiveRecord::Migration[6.1]
  def change
    change_column :deploys, :rol_id, :integer, null: true
  end
end
