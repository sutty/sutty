# frozen_string_literal: true

# No podemos compartir el uuid entre indexed_posts y posts porque
# podemos tener sitios duplicados.  Al menos hasta que los sitios de
# testeo estén integrados en el panel vamos a tener que generar otros
# UUID.
class IndexedPostsByUuidAndSiteId < ActiveRecord::Migration[6.1]
  def up
    add_column :indexed_posts, :post_id, :uuid, index: true

    IndexedPost.transaction do
      ActiveRecord::Base.connection.execute('update indexed_posts set post_id = id where post_id is null')
    end
  end

  def down
    remove_column :indexed_posts, :post_id
  end
end
