# frozen_string_literal: true

# Los sitios tienen un diseño
class AddDesignToSites < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :sites, :design, index: true
  end
end
