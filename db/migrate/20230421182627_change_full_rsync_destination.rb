# frozen_string_literal: true

# Envía los cambios a través de rsyncd
class ChangeFullRsyncDestination < ActiveRecord::Migration[6.1]
  def up
    DeployFullRsync.find_each do |deploy|
      Rails.application.nodes.each do |node|
        deploy.destination = "rsync://rsyncd.#{node}/deploys/"
        deploy.save
      end
    end
  end

  def down
    DeployFullRsync.find_each do |deploy|
      Rails.application.nodes.each do |node|
        deploy.destination = "sutty@#{node}:"
        deploy.save
      end
    end
  end
end
