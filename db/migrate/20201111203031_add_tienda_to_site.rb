class AddTiendaToSite < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :tienda_api_key_ciphertext, :string, default: ''
    add_column :sites, :tienda_url, :string, default: ''
  end
end
