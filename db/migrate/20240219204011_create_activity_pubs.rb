# frozen_string_literal: true

# Registro de actividades.
class CreateActivityPubs < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pubs, id: :uuid do |t|
      t.timestamps

      t.bigint :site_id, null: false
      t.uuid :object_id, null: false
      t.string :object_type, null: false

      t.string :aasm_state, null: false

      t.index %i[site_id object_id object_type], unique: true
    end
  end
end
