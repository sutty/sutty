# frozen_string_literal: true

# Agrega la columna de request_uri a la tabla de logs
class AddRequestUriToAccessLogs < ActiveRecord::Migration[6.1]
  def change
    return unless Rails.env.production?

    add_column :access_logs, :request_uri, :string, default: ''
  end
end
