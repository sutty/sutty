# frozen_string_literal: true

# Crea una tabla de avisos de mantenimiento
class CreateMaintenance < ActiveRecord::Migration[6.0]
  def change
    create_table :maintenances do |t|
      t.timestamps
      t.text :message
      t.datetime :estimated_from
      t.datetime :estimated_to
      t.boolean :are_we_back, default: false
    end
  end
end
