class ChangeRefererToOrigin < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    rename_column :access_logs, :http_referer, :http_origin

    # Eliminar todos los referers anteriores manteniendo el origin
    ActiveRecord::Base.connection.execute("update access_logs set http_origin = split_part(http_origin, '/', 1) || '//' || split_part(http_origin, '/', 3) where http_origin is not null")
  end
end
