# frozen_string_literal: true

# Los sitios alojados en Distributed Press son públicos por defecto y no
# tienen HTTP, porque lo hace Sutty
class DistributedSitesArePublic < ActiveRecord::Migration[6.1]
  def up
    require 'distributed_press/v1/schemas/update_site'

    DeployDistributedPress.all.reject do |deploy|
      deploy.values.dig(*%w[remote_info distributed_press public]).present?
    end.map do |deploy|
      update_site = DistributedPress::V1::Schemas::UpdateSite.new.call(id: deploy.hostname, public: true, protocols: { http: false, hyper: true, ipfs: true })
      deploy.send(:site_client).update(update_site)
      updated_site = deploy.send(:site_client).show(update_site)

      deploy.remote_info[:distributed_press] = updated_site.to_h
      deploy.save
    end
  end

  def down; end
end
