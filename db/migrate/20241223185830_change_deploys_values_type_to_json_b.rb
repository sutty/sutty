# frozen_string_literal: true

class ChangeDeploysValuesTypeToJsonB < ActiveRecord::Migration[6.1]
  def up
    add_column :deploys, :values_2, :jsonb, default: {}

    Deploy.find_each do |deploy|
      deploy.update values_2: JSON.parse(deploy.values)
    end

    remove_column :deploys, :values
    rename_column :deploys, :values_2, :values
  end

  def down
    add_column :deploys, :values_2, :text

    Deploy.find_each do |deploy|
      deploy.update values_2: deploy.values.to_json
    end

    remove_column :deploys, :values
    rename_column :deploys, :values_2, :values
  end
end
