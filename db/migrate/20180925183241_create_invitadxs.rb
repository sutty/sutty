# frozen_string_literal: true

class CreateInvitadxs < ActiveRecord::Migration[5.1]
  def change
    create_table :invitadxs do |t|
      t.timestamps
      t.string :password_digest
      t.string :email, unique: true, index: true
      t.string :confirmation_token
      t.boolean :confirmed
    end
  end
end
