# frozen_string_literal: true

# Relaciona estados de actividades con les actores que las hicieron
class AddActorIdToActivityPubs < ActiveRecord::Migration[6.1]
  def up
    add_column :activity_pubs, :actor_id, :uuid

    ActivityPub.all.find_each do |activity_pub|
      activity_pub.update_column(:actor_id, activity_pub.activities.last.actor_id)
    end
  end

  def down
    remove_column :activity_pubs, :actor_id
  end
end
