# frozen_string_literal: true

# Elimina actores que no pudieron ser eliminades porque su perfil ya no
# existe.
class RemoveActorModerationsForGenericObjects < ActiveRecord::Migration[6.1]
  def up
    object_ids = ActivityPub.removed.where(object_type: 'ActivityPub::Object::Generic').distinct.pluck(:object_id)
    uris = ActivityPub::Object.where(id: object_ids).pluck(:uri)
    actor_ids = ActivityPub::Actor.where(uri: uris).ids

    ActorModeration.where(actor_id: actor_ids).remove_all!
  end

  def down; end
end
