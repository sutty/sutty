# frozen_string_literal: true

# Algunes quedaron como genéricxs
class RemoveActorModerations2 < ActiveRecord::Migration[6.1]
  def up
    actor_uris = ActivityPub::Activity.unscope(:order).where(type: 'ActivityPub::Activity::Delete').distinct.pluck(Arel.sql("content->>'object'"))
    actor_ids = ActivityPub::Actor.where(uri: actor_uris).ids

    ActorModeration.where(actor_id: actor_ids).remove_all!
  end

  def down; end
end
