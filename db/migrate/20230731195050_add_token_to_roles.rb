class AddTokenToRoles < ActiveRecord::Migration[6.1]
  def up
    add_column :roles, :token, :string
    Rol.find_each do |m|
      m.update_column( :token, SecureRandom.hex(64) ) 
    end
  end

  def down
    remove_column :roles, :token 
  end
end
