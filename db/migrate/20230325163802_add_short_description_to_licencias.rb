# frozen_string_literal: true

# Agrega descripciones cortas a las licencias
class AddShortDescriptionToLicencias < ActiveRecord::Migration[6.1]
  def up
    add_column :licencias, :short_description, :string

    YAML.safe_load_file('db/seeds/licencias.yml').each do |licencia|
      Licencia.find_by_icons(licencia['icons']).update licencia
    end
  end

  def down
    remove_column :licencias, :short_description
  end
end
