# frozen_string_literal: true

# De alguna forma se guardaron objetos duplicados!
class FixDuplicateObjects < ActiveRecord::Migration[6.1]
  def up
    ActivityPub::Object.group(:uri).count.select { |_, v| v > 1 }.each_key do |uri|
      objects = ActivityPub::Object.where(uri: uri)
      deleted_ids = objects[1..].map(&:delete).map(&:id)

      ActivityPub.where(object_id: deleted_ids).update_all(object_id: objects.first.id, updated_at: Time.now)
    end
  end

  def down; end
end
