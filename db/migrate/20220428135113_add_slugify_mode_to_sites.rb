# frozen_string_literal: true

# Permite a los sitios elegir el método de slugificación
class AddSlugifyModeToSites < ActiveRecord::Migration[6.1]
  def change
    add_column :sites, :slugify_mode, :string, default: 'default'
  end
end
