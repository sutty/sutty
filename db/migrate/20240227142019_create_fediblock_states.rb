# frozen_string_literal: true

# La relación entre sitios y fediblocks
class CreateFediblockStates < ActiveRecord::Migration[6.1]
  def up
    create_table :fediblock_states, id: :uuid do |t|
      t.timestamps

      t.belongs_to :site
      t.uuid :fediblock_id, index: true
      t.string :aasm_state

      t.index %i[site_id fediblock_id], unique: true
    end

    # Todas las listas están activas por defecto
    DeploySocialDistributedPress.find_each do |deploy|
      ActivityPub::Fediblock.find_each do |fediblock|
        FediblockState.create(site: deploy.site, fediblock: fediblock, aasm_state: 'disabled').tap(&:enable!)
      end
    end
  end

  def down
    drop_table :fediblock_states
  end
end
