# frozen_string_literal: true

class ChangeLastModified < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    change_column :access_logs, :sent_http_last_modified, :string
  end
end
