# frozen_string_literal: true

designs = YAML.safe_load(File.read('db/seeds/designs.yml'))

designs.each do |d|
  design = Design.find_or_create_by(gem: d['gem'])

  design.update d
end

licencias = YAML.safe_load(File.read('db/seeds/licencias.yml'))

licencias.each do |l|
  licencia = Licencia.find_or_create_by(icons: l['icons'])

  licencia.update l
end

if CodeOfConduct.count.zero?
  YAML.safe_load(File.read('db/seeds/codes_of_conduct.yml')).each do |coc|
    CodeOfConduct.new(**coc).save!
  end
end

if PrivacyPolicy.count.zero?
  YAML.safe_load(File.read('db/seeds/privacy_policies.yml')).each do |pp|
    PrivacyPolicy.new(**pp).save!
  end
end

YAML.safe_load(File.read('db/seeds/activity_pub/fediblocks.yml')).each do |fediblock|
  ActivityPub::Fediblock.find_or_create_by(id: fediblock['id']).tap do |f|
    f.update(**fediblock)
  end
end
