# frozen_string_literal: true

source ENV.fetch('GEMS_SOURCE', 'https://gems.sutty.nl')

ruby "~> #{ENV.fetch('RUBY_VERSION', '3.1')}"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.0'
# Use Puma as the app server
gem 'puma'

# Solo incluir las gemas cuando estemos en desarrollo o compilando los
# assets.  No es necesario instalarlas en producción.
#
# XXX: Supuestamente Rails ya soporta RAILS_GROUPS, pero Bundler no.
if ENV['RAILS_GROUPS']&.split(',')&.include? 'assets'
  gem 'sassc-rails'
  gem 'uglifier', '>= 1.3.0'
  gem 'bootstrap', '~> 4'
end

gem 'nokogiri'
gem 'rgl'

# Turbolinks makes navigating your web application faster. Read more:
# https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more:
# https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
gem 'safely_block', '~> 0.3.0'
gem 'blazer'
gem 'chartkick'
gem 'commonmarker'
gem 'devise'
gem 'devise-i18n'
gem 'devise_invitable'
gem 'redis-client'
gem 'hiredis-client'
gem 'distributed-press-api-client', '~> 0.4.1'
gem 'email_address', git: 'https://github.com/fauno/email_address', branch: 'i18n'
gem 'exception_notification'
gem 'fast_blank'
gem 'friendly_id'
gem 'hamlit-rails'
gem 'hiredis'
gem 'image_processing'
gem 'icalendar'
gem 'inline_svg'
gem 'httparty'
gem 'safe_yaml', require: false
gem 'jekyll', '~> 4.2.0'
gem 'jekyll-commonmark', '~> 1.4.0'
gem 'jekyll-images'
gem 'jekyll-include-cache'
gem 'sutty-liquid', '>= 0.7.3'
gem 'loaf'
gem 'lockbox'
gem 'mini_magick'
gem 'mobility'
gem 'pundit'
gem 'rails-i18n'
gem 'rails_warden'
gem 'redis', '~> 4.0', require: %w[redis redis/connection/hiredis]
gem 'redis-rails'
gem 'rollups', git: 'https://github.com/fauno/rollup.git', branch: 'update'
gem 'rubyzip'
gem 'ruby-brs'
gem 'rugged', '1.5.0.1'
gem 'git_clone_url'
gem 'concurrent-ruby-ext'
gem 'que'
gem 'symbol-fstring', require: 'fstring/all'
gem 'terminal-table'
gem 'validates_hostname'
gem 'webpacker'
gem 'yaml_db', git: 'https://0xacab.org/sutty/yaml_db.git'
gem 'kaminari'
gem 'device_detector'
gem 'htmlbeautifier'
gem 'dry-schema'
gem 'rubanok'

gem 'after_commit_everywhere', '~> 1.0'
gem 'aasm'
gem 'que-web'

# database
gem 'hairtrigger'
gem 'pg'
gem 'pg_search'

# performance
gem 'flamegraph'
gem 'memory_profiler'
gem 'rack-mini-profiler'
gem 'stackprof'
gem 'prometheus_exporter'

# debug
gem 'fast_jsonparser', '~> 0.5.0'
gem 'down'
gem 'sourcemap'
gem 'rack-cors'

# ssh
gem 'net-ssh'
gem 'ed25519'
gem 'bcrypt_pbkdf'

group :production do
  gem 'lograge'
end

group :development, :test do
  gem 'derailed_benchmarks'
  gem 'dotenv-rails'
  gem 'pry'
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver', '~> 4.8.0'
  gem 'sqlite3'
end

group :development do
  gem 'yard'
  gem 'brakeman'
  gem 'bundler-audit'
  gem 'haml-lint', require: false
  gem 'letter_opener'
  gem 'listen'
  gem 'rubocop-rails'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'web-console'
end

group :test do
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'timecop'
end
