# frozen_string_literal: true

class DesignTest < ActiveSupport::TestCase
  test 'se pueden crear' do
    design = create :design

    assert design.valid?
    assert design.persisted?
  end
end
