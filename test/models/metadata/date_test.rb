# frozen_string_literal: true

require 'test_helper'

module Metadata
  class DateTest < ActiveSupport::TestCase
    DocumentStub = Struct.new(:data, keyword_init: true)

    attr_reader :document, :metadata

    setup do
      @layout = { 'field' => { 'type' => 'date' } }
      @document = DocumentStub.new(data: { 'field' => random_date })
      @metadata = MetadataDate.new(document: @document, name: :field)
    end

    test 'la fecha por defecto es hoy' do
      assert_equal Date.today, metadata.default_value
    end

    test 'si el documento tiene un valor usamos ese' do
      assert_equal document.data['field'], metadata.value
    end

    test 'si el documento no tiene un valor usamos la fecha por defecto' do
      document.data.delete 'field'

      assert_equal Date.today, metadata.value
    end

    test 'si le damos un fecha mantenemos esa' do
      date = random_date
      metadata.value = date

      assert_equal date, metadata.value
    end

    test 'si le damos un texto obtenemos una fecha' do
      date = random_date.to_date
      metadata.value = date.strftime('%F')

      assert_equal date, metadata.value
    end

    private

    def random_date
      (SecureRandom.random_number * 100).to_i.months.ago
    end
  end
end
