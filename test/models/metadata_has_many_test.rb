# frozen_string_literal: true

require 'test_helper'
require_relative 'metadata_test'

class MetadataHasManyTest < ActiveSupport::TestCase
  include MetadataTest

  test 'se pueden relacionar artículos' do
    reply = @site.posts.create(layout: :post, title: SecureRandom.hex)
    post = @site.posts.create(layout: :post, title: SecureRandom.hex, posts: [reply.uuid.value])

    assert_equal post, reply.in_reply_to.belongs_to
    assert_includes post.posts.has_many, reply

    assert reply.save

    assert_equal reply.document.data['in_reply_to'], post.document.data['uuid']
    assert_includes post.document.data['posts'], reply.document.data['uuid']
  end

  test 'se puede eliminar la relación' do
    reply = @site.posts.create(layout: :post, title: SecureRandom.hex)
    post = @site.posts.create(layout: :post, title: SecureRandom.hex, posts: [reply.uuid.value])

    post.posts.value = []
    assert post.save

    assert_not_equal post, reply.in_reply_to.belongs_to
    assert_equal post, reply.in_reply_to.belonged_to
    assert_nil reply.in_reply_to.belongs_to
    assert_not_includes post.posts.has_many, reply

    assert reply.save

    assert_nil reply.document.data['in_reply_to']
    assert_not_includes post.document.data['posts'], reply.document.data['uuid']
  end

  test 'se puede cambiar la relación' do
    reply = @site.posts.create(layout: :post, title: SecureRandom.hex)
    post1 = @site.posts.create(layout: :post, title: SecureRandom.hex, posts: [reply.uuid.value])
    post2 = @site.posts.create(layout: :post, title: SecureRandom.hex)

    reply.in_reply_to.value = post2.uuid.value
    assert reply.save

    assert_not_equal post1, reply.in_reply_to.belongs_to
    assert_equal post1, reply.in_reply_to.belonged_to
    assert_not_includes post1.posts.has_many, reply

    assert_equal post2, reply.in_reply_to.belongs_to
    assert_includes post2.posts.has_many, reply

    assert post1.save
    assert post2.save

    assert_equal post2.document.data['uuid'], reply.document.data['in_reply_to']
    assert_includes post2.document.data['posts'], reply.document.data['uuid']
    assert_not_includes post1.document.data['posts'], reply.document.data['uuid']
  end
end
