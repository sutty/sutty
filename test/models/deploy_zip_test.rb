# frozen_string_literal: true

require 'test_helper'

class DeployZipTest < ActiveSupport::TestCase
  test 'se puede deployear' do
    deploy_local = create :deploy_local

    assert deploy_local.deploy
    assert File.directory?(deploy_local.destination)
    assert File.exist?(File.join(deploy_local.destination, 'index.html'))
    assert_equal 3, deploy_local.build_stats.count

    assert deploy_local.build_stats.map(&:bytes).compact.inject(:+).positive?
    assert deploy_local.build_stats.map(&:seconds).compact.inject(:+).positive?

    assert deploy_local.destroy
    assert_not File.directory?(deploy_local.destination)
  end
end
