# frozen_string_literal: true

class DeployLocalTest < ActiveSupport::TestCase
  test 'se puede deployear' do
    site = create :site
    local = create :deploy_local, site: site
    deploy = create :deploy_zip, site: site

    # Primero tenemos que generar el sitio
    local.deploy

    escaped_path = Shellwords.escape(deploy.path)

    assert deploy.deploy
    assert File.file?(deploy.path)
    assert_equal 'application/zip',
                 `file --mime-type "#{escaped_path}"`.split(' ').last
    assert_equal 1, deploy.build_stats.count
    assert deploy.build_stats.map(&:bytes).inject(:+).positive?
    assert deploy.build_stats.map(&:seconds).inject(:+).positive?

    local.destroy
  end
end
