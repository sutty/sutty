# frozen_string_literal: true

module MetadataTest
  extend ActiveSupport::Concern

  included do
    setup do
      name = SecureRandom.hex
      # TODO: Poder cambiar el nombre
      FileUtils.cp_r(Rails.root.join('test', 'fixtures', 'site_with_relationships'), Rails.root.join('_sites', name))

      @site = create :site, name: name
    end

    teardown do
      @site&.destroy
    end
  end
end
