# frozen_string_literal: true

require 'test_helper'

class RepositoryTest < ActiveSupport::TestCase
  setup do
    @rol = create :rol
    @site = @rol.site
    @usuarie = @rol.usuarie

    # Volver al principio para poder traer cambios
    Dir.chdir(@site.path) do
      `git reset --hard e0627e34c6ef6ae2592d7f289b82def20ba56685`
    end
  end

  teardown do
    @site.destroy
  end

  test 'se puede obtener la rama por defecto' do
    assert_equal 'master', @site.repository.default_branch

    random_branch = SecureRandom.hex

    Dir.chdir(@site.path) do
      `git checkout -b #{random_branch} >/dev/null 2>&1`
    end

    assert_equal random_branch, @site.repository.default_branch
  end

  test 'los nombres de las ramas pueden tener /' do
    random_branch = ([SecureRandom.hex] * 2).join('/')

    Dir.chdir(@site.path) do
      `git checkout -b #{random_branch} >/dev/null 2>&1`
    end

    assert_equal random_branch, @site.repository.default_branch
  end

  # XXX: En realidad el git-reset no elimina la caché de fetch entonces
  # nunca tenemos valores mayores a cero.
  test 'se pueden traer cambios' do
    assert @site.repository.fetch.zero?
  end

  test 'se pueden mergear los cambios' do
    assert !@site.repository.commits.empty?
    assert @site.repository.merge(@usuarie)
    assert @site.repository.commits.empty?

    assert_equal @usuarie.name,
                 @site.repository.rugged.branches[@site.repository.default_branch].target.author[:name]

    Dir.chdir(@site.path) do
      FileUtils.rm 'migration.csv'

      assert_equal 'nothing to commit, working tree clean',
                   `LC_ALL=C git status`.strip.split("\n").last
    end
  end
end
