# frozen_string_literal: true

class ConstraintsApiSubdomainTest < ActiveSupport::TestCase
  setup do
    @constraint = Constraints::ApiSubdomain.new

    MockRequest = Struct.new(:hostname, keyword_init: true) unless defined? MockRequest
  end

  test 'cualquier subdominio que empiece con api. matchea' do
    request = MockRequest.new hostname: 'api.'

    (rand * 10).to_i.times do
      request.hostname += "#{SecureRandom.hex}."

      assert @constraint.matches?(request)
    end
  end

  test 'cualquier subdominio que no empiece con api. matchea' do
    request = MockRequest.new hostname: 'panel.'

    (rand * 10).to_i.times do
      request.hostname += "#{SecureRandom.hex}."

      assert_not @constraint.matches?(request)
    end
  end
end
