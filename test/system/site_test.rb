# frozen_string_literal: true

require 'test_helper'

class SiteTest < ActiveSupport::TestCase
  def site
    @site ||= Site.new name: 'recursero', design_id: Design.find_by_gem('recursero-jekyll-theme').id
  end

  test 'tiene una performance' do
    skip unless ENV['TEST_PERFORMANCE'].present?

    site.read

    profile = StackProf.run mode: :object do
      site.posts
    end

    StackProf::Report.new(profile).print_text
  end
end
