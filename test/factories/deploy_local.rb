# frozen_string_literal: true

FactoryBot.define do
  factory :deploy_local do
    site
  end
end
