# frozen_string_literal: true

FactoryBot.define do
  factory :deploy_zip do
    site
  end
end
