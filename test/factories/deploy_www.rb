# frozen_string_literal: true

FactoryBot.define do
  factory :deploy_www do
    site
  end
end
