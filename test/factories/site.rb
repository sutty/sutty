# frozen_string_literal: true

FactoryBot.define do
  factory :site do
    name { "test-#{SecureRandom.hex}" }
    title { SecureRandom.hex }
    description { SecureRandom.hex * 2 }
    design
    licencia

    after :build do |site|
      site.deploys << build(:deploy_local, site: site)
    end

    after :create do |site|
      site.deploys << create(:deploy_local, site: site)
    end
  end
end
