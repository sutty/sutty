# frozen_string_literal: true

FactoryBot.define do
  factory :licencia do
    name { SecureRandom.hex }
    description { SecureRandom.hex }
    url { SecureRandom.hex }
    deed { SecureRandom.hex }
    icons { '/images/by.png' }
  end
end
