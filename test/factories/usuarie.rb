# frozen_string_literal: true

FactoryBot.define do
  factory :usuarie do
    email { SecureRandom.hex + '@sutty.nl' }
    password { SecureRandom.hex }
    confirmed_at { Date.today }
    lang { I18n.default_locale.to_s }
  end
end
