# frozen_string_literal: true

FactoryBot.define do
  factory :design do
    name { SecureRandom.hex }
    description { SecureRandom.hex }
    license { SecureRandom.hex }
    gem { SecureRandom.hex }
    url { SecureRandom.hex }
    disabled { false }
  end
end
