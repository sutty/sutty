# frozen_string_literal: true

require File.expand_path('../config/environment', __dir__)
require 'rails/test_help'
require 'open3'
require 'sucker_punch/testing/inline'

Rails.application.load_seed

# rubocop:disable Style/ClassAndModuleChildren
class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods
  # Resetear el repositorio a su estado original antes de leerlo
  #
  # TODO mover a Site.reset! cuando empecemos a trabajar con git
  def reset_git_repo(path)
    Dir.chdir(path) do
      Open3.popen3('git reset --hard') do |_, _, _, thread|
        # Wait for the process to finish
        thread.value
      end
    end
  end
end
# rubocop:enable Style/ClassAndModuleChildren
