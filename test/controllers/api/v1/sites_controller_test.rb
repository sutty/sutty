# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class SitesControllerTest < ActionDispatch::IntegrationTest
      setup do
        @rol = create :rol
        @site = @rol.site
        @usuarie = @rol.usuarie

        @authorization = {
          Authorization: ActionController::HttpAuthentication::Basic
                         .encode_credentials(ENV['HTTP_BASIC_USER'],
                                             ENV['HTTP_BASIC_PASSWORD'])
        }
      end

      teardown do
        @site.destroy
      end

      test 'se puede obtener un listado de todos' do
        get v1_sites_url(host: "api.#{Site.domain}"), headers: @authorization, as: :json
        assert_equal Site.all.pluck(:name), JSON.parse(response.body)
      end
    end
  end
end
