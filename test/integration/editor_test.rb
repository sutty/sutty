# frozen_string_literal: true

require 'test_helper'

class EditorTest < ActionDispatch::IntegrationTest
  setup do
    @rol = create :rol
    @site = @rol.site
    @usuarie = @rol.usuarie

    @authorization = {
      Authorization: ActionController::HttpAuthentication::Basic
                     .encode_credentials(@usuarie.email, @usuarie.password)
    }
  end

  teardown do
    @site.destroy
  end

  test 'al enviar html se guarda markdown' do
    skip
    content = <<~CONTENT
      <h1>Hola</h1>

      <p>Qué <em>onda?</em>, <strong>estamos al aire</strong></p>

      <blockquote>Una cita</blockquote>

      <img src="https://sutty.nl/logo.png" alt="Logo de Sutty"/>
    CONTENT

    md_content = <<~MARKDOWN
      # Hola

      Qué _onda?_, **estamos al aire**

      > Una cita

       ![Logo de Sutty](https://sutty.nl/logo.png)
    MARKDOWN

    post site_posts_url(@site), headers: @authorization,
                                params: {
                                  post: {
                                    title: SecureRandom.hex,
                                    description: SecureRandom.hex,
                                    content: content
                                  }
                                }

    @post = @site.posts.first

    assert_equal md_content.strip, @post.content.value
  end

  test 'convertir trix' do
    skip
    path      = Rails.root.join('test', 'fixtures', 'files')
    trix_orig = File.read(File.join(path, 'trix.txt'))
    trix_md   = File.read(File.join(path, 'trix.md'))

    post site_posts_url(@site), headers: @authorization,
                                params: {
                                  post: {
                                    title: SecureRandom.hex,
                                    description: SecureRandom.hex,
                                    content: trix_orig
                                  }
                                }

    @post = @site.posts.first

    assert_equal trix_md.strip, @post.content.value
  end
end
