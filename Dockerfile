ARG RUBY_VERSION=3.1
ARG RUBY_PATCH=4
ARG ALPINE_VERSION=3.17.3
ARG BASE_IMAGE=registry.nulo.in/sutty/rails
FROM ${BASE_IMAGE}:${ALPINE_VERSION}-${RUBY_VERSION}.${RUBY_PATCH}
ARG PANDOC_VERSION=2.18
ENV RAILS_ENV production

# Instalar las dependencias, separamos la librería de base de datos para
# poder reutilizar este primer paso desde otros contenedores
#
# Necesitamos yarn para que Jekyll pueda generar los sitios
# XXX: Eliminarlo cuando extraigamos la generación de sitios del proceso
# principal
RUN apk add --no-cache libxslt libxml2 postgresql-libs libssh2 \
      rsync git jpegoptim vips tectonic oxipng git-lfs openssh-client \
      yarn daemonize ruby-webrick postgresql-client dateutils file

RUN gem install --no-document --no-user-install foreman
RUN wget https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-linux-amd64.tar.gz -O - | tar --strip-components 1 -xvzf - pandoc-${PANDOC_VERSION}/bin/pandoc && mv /bin/pandoc /usr/bin/pandoc
RUN apk add npm && npm install -g pnpm@~7 && apk del npm

COPY ./monit.conf /etc/monit.d/sutty.conf

RUN apk add npm && npm install -g pnpm && apk del npm

VOLUME "/srv"

EXPOSE 3000
EXPOSE 9394
