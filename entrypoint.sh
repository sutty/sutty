#!/bin/sh
set -e

s_pid=/srv/tmp/puma.pid
p_pid=/tmp/prometheus.pid

case $1 in
  start)
    su rails -c "cd /srv && foreman run migrate"
    daemonize -c /srv -u rails /usr/bin/foreman start sutty
    ;;

  stop)
    cat $s_pid | xargs -r kill
    ;;

  reload)
    cat $s_pid | xargs -r kill -USR2
    ;;

  prometheus)
    case $2 in
      start)
        rm -f $p_pid
        daemonize -c /srv -p $p_pid -l $p_pid -u rails /usr/bin/foreman start prometheus
        ;;
      stop)
        cat $p_pid | xargs -r kill
        rm -f $p_pid
      ;;
    esac
    ;;

  blazer)
    test -z "$2" || b="_$2"
    su rails -c "cd /srv && foreman run blazer$b"
    ;;
esac
